view: franchise_owner_information {
  derived_table: {
    sql: select f.id AS Franchise_id
      ,f.contract_id
      ,a.id AS Agent_id
      ,f.start_date
      ,CONCAT(a.first_name, CONCAT(' ',a.last_name)) AS Agent_name
      ,aed.email AS Owner_email
      ,f.training_date AS Training_date
      ,a.work_phone AS Work_phone
      ,a.home_phone AS Home_phone
      ,a.cell_phone AS Cell_phone
      -- ,MAX(cte.created) AS last_coaching_call
      ,max(case when (cte.call_type = 'outbound_coaching'
                     AND cte.left_voicemail = 0
                     AND cte.duration != '< 5 mins') then cte.created else null end) AS last_coaching_call
      FROM excolo.agents a
      INNER JOIN excolo.franchises f ON a.franchise_id=f.id
      LEFT JOIN excolo.call_tracker_entries cte ON a.id=cte.agent_id
      LEFT JOIN excolo.agent_email_addresses aed ON aed.agent_id = a.id AND a.agent_email_address_id = aed.id

      WHERE f.deleted=0
            AND a.deleted=0
            AND a.terminated=0
            AND cast(coalesce(nullif(f.contract_id,''), '0') as integer) >=8000
            AND f.franchise_status_id IN (1,2)
            -- AND coalesce(cte.call_type,'') IN ('outbound_coaching','')
            -- AND cte.call_type IN ('outbound_coaching')
            AND a.primary = 1 AND a.owner = 1
      GROUP BY f.id,f.contract_id,f.start_date,a.id,CONCAT(a.first_name, CONCAT(' ',a.last_name)),aed.email,f.training_date,a.work_phone
               ,a.home_phone,a.cell_phone
      ORDER BY a.id

                   ;;
   # persist_for: "1 hours" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours
    #distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

  }
  # DIMENSIONS #
  dimension: Franchise_id {
    type: number
    primary_key: yes
    sql: ${TABLE}.Franchise_id ;;
  }

  dimension: Agent_id {
    type: number
    sql: ${TABLE}.Agent_id ;;
  }

  dimension: Contract_Signed {
    type: date
    sql: ${TABLE}.start_date ;;
  }

  dimension: agent_call_tracker {
    type: string
    sql: CONCAT('https://mcp.cruiseplannersnet.com/admin/call_tracker_entries/index/',${Agent_id}) ;;
  }

  dimension: agent_url {
    type: string
    sql: CONCAT('https://mcp.cruiseplannersnet.com/admin/agents/view/',${Agent_id}) ;;
  }

  dimension: franchise_url {
    type: string
    sql: CONCAT('https://mcp.cruiseplannersnet.com/admin/franchises/view/',${Franchise_id}) ;;
  }

  dimension: Owner_name {
    type: string
    sql: ${TABLE}.Agent_name  ;;
    link: {
      label: "View Agent on MCP"
      url: "{{agent_url}}"
    }
    link: {
      label: "View Franchise on MCP"
      url: "{{franchise_url}}"
    }
    link: {
      label: "View Agent's Call Tracker"
      url: "{{agent_call_tracker}}"
    }

  }

  dimension: Owner_email {
    type: string
    sql: ${TABLE}.Owner_email  ;;
  }

  dimension: Training_date {
    type: date
    sql: ${TABLE}.Training_date ;;
  }

  dimension: Work_phone {
    type: string
    sql: ${TABLE}.Work_phone ;;
  }

  dimension: Home_phone {
    type: string
    sql: ${TABLE}.Home_phone ;;
  }

  dimension: Cell_phone {
    type: string
    sql: ${TABLE}.Cell_phone ;;
  }

  dimension: last_valid_coaching_call {
    type: date
    sql: ${TABLE}.last_coaching_call ;;
  }




}
