view: agents {
  sql_table_name: excolo.agents ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: _sdc_batched {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_batched_at ;;
  }

  dimension_group: _sdc_extracted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_extracted_at ;;
  }

  dimension_group: _sdc_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_received_at ;;
  }

  dimension: _sdc_sequence {
    type: number
    sql: ${TABLE}._sdc_sequence ;;
  }

  dimension: _sdc_table_version {
    type: number
    sql: ${TABLE}._sdc_table_version ;;
  }

  dimension: agent_email_address_id {
    type: number
    sql: ${TABLE}.agent_email_address_id ;;
  }

  dimension: agent_group_id {
    type: number
    sql: ${TABLE}.agent_group_id ;;
  }

  dimension_group: agent_merged {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.agent_merged_date ;;
  }

  dimension: agent_type {
    type: number
    sql: ${TABLE}.agent_type ;;
  }

  dimension: alternate_email_address {
    type: string
    sql: ${TABLE}.alternate_email_address ;;
  }

  dimension: associate_program_access {
    type: string
    sql: ${TABLE}.associate_program_access ;;
  }

  dimension_group: associate_program_access_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_access_end ;;
  }

  dimension_group: associate_program_access_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_access_start ;;
  }

  dimension_group: associate_program_brochure_rack_enrollment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_brochure_rack_enrollment_date ;;
  }

  dimension_group: associate_program_brochure_rack_expiration {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_brochure_rack_expiration_date ;;
  }

  dimension_group: associate_program_brochure_rack_next_billing {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_brochure_rack_next_billing_date ;;
  }

  dimension: associate_program_brochure_rack_status {
    type: string
    sql: ${TABLE}.associate_program_brochure_rack_status ;;
  }

  dimension: associate_program_cc_id {
    type: number
    sql: ${TABLE}.associate_program_cc_id ;;
  }

  dimension: associate_program_cc_type {
    type: string
    sql: ${TABLE}.associate_program_cc_type ;;
  }

  dimension_group: associate_program_enrollment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_enrollment_date ;;
  }

  dimension_group: associate_program_expiration {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_expiration_date ;;
  }

  dimension: associate_program_fee {
    type: number
    sql: ${TABLE}.associate_program_fee ;;
  }

  dimension_group: associate_program_next_billing {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_next_billing_date ;;
  }

  dimension: associate_program_pdf_id {
    type: number
    sql: ${TABLE}.associate_program_pdf_id ;;
  }

  dimension: associate_program_receive_ho_news {
    type: yesno
    sql: ${TABLE}.associate_program_receive_ho_news ;;
  }

  dimension: associate_program_sendible_content_type {
    type: string
    sql: ${TABLE}.associate_program_sendible_content_type ;;
  }

  dimension_group: associate_program_sendible_enrollment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_sendible_enrollment_date ;;
  }

  dimension_group: associate_program_sendible_expiration {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_sendible_expiration_date ;;
  }

  dimension: associate_program_sendible_facebook_page {
    type: string
    sql: ${TABLE}.associate_program_sendible_facebook_page ;;
  }

  dimension_group: associate_program_sendible_next_billing {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.associate_program_sendible_next_billing_date ;;
  }

  dimension: associate_program_sendible_status {
    type: string
    sql: ${TABLE}.associate_program_sendible_status ;;
  }

  dimension: associate_program_status {
    type: string
    sql: ${TABLE}.associate_program_status ;;
  }

  dimension: associate_program_subdomain {
    type: string
    sql: ${TABLE}.associate_program_subdomain ;;
  }

  dimension: associate_program_website_id {
    type: number
    sql: ${TABLE}.associate_program_website_id ;;
  }

  dimension: billing_status_id {
    type: number
    sql: ${TABLE}.billing_status_id ;;
  }

  dimension_group: birthday {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.birthday ;;
  }

  dimension: booking_code {
    type: string
    sql: ${TABLE}.booking_code ;;
  }

  dimension: cell_phone {
    type: string
    sql: ${TABLE}.cell_phone ;;
  }

  dimension: certification {
    type: string
    sql: ${TABLE}.certification ;;
  }

  dimension: cfr_enable_notice {
    type: yesno
    sql: ${TABLE}.cfr_enable_notice ;;
  }

  dimension: cfr_exclude_paid_cabins {
    type: yesno
    sql: ${TABLE}.cfr_exclude_paid_cabins ;;
  }

  dimension: chat_dnd {
    type: yesno
    sql: ${TABLE}.chat_dnd ;;
  }

  dimension: chat_welcome_msg {
    type: string
    sql: ${TABLE}.chat_welcome_msg ;;
  }

  dimension: cim_profile_id {
    type: string
    sql: ${TABLE}.cim_profile_id ;;
  }

  dimension: clients_field_order {
    type: string
    sql: ${TABLE}.clients_field_order ;;
  }

  dimension: clients_optedin_count {
    type: number
    sql: ${TABLE}.clients_optedin_count ;;
  }

  dimension_group: clients_optedin {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.clients_optedin_date ;;
  }

  dimension: contact_photo {
    type: string
    sql: ${TABLE}.contact_photo ;;
  }

  dimension: contract_id {
    type: string
    sql: ${TABLE}.contract_id ;;
  }

  dimension: cpcom_admin {
    type: yesno
    sql: ${TABLE}.cpcom_admin ;;
  }

  dimension: cpsoft_or_cpnet_home {
    type: yesno
    sql: ${TABLE}.cpsoft_or_cpnet_home ;;
  }

  dimension: cpsoftid {
    type: string
    sql: ${TABLE}.cpsoftid ;;
  }

  dimension_group: cpv_last_viewed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.cpv_last_viewed ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: dashboard_configuration {
    type: string
    sql: ${TABLE}.dashboard_configuration ;;
  }

  dimension: dashboard_links {
    type: string
    sql: ${TABLE}.dashboard_links ;;
  }

  dimension: data_validated {
    type: yesno
    sql: ${TABLE}.data_validated ;;
  }

  dimension: data_validation_ignored {
    type: number
    sql: ${TABLE}.data_validation_ignored ;;
  }

  dimension: debugging {
    type: yesno
    sql: ${TABLE}.debugging ;;
  }

  dimension: default_cruise_sheet_email_message {
    type: string
    sql: ${TABLE}.default_cruise_sheet_email_message ;;
  }

  dimension: default_cruise_sheet_email_subject {
    type: string
    sql: ${TABLE}.default_cruise_sheet_email_subject ;;
  }

  dimension: default_cruise_sheet_include_shoretrips {
    type: yesno
    sql: ${TABLE}.default_cruise_sheet_include_shoretrips ;;
  }

  dimension: default_cruise_sheet_message_for_client {
    type: string
    sql: ${TABLE}.default_cruise_sheet_message_for_client ;;
  }

  dimension: default_cruise_sheet_show_category_total {
    type: yesno
    sql: ${TABLE}.default_cruise_sheet_show_category_total ;;
  }

  dimension: default_fac_display_type {
    type: string
    sql: ${TABLE}.default_fac_display_type ;;
  }

  dimension: default_fac_only_us_departure_ports {
    type: yesno
    sql: ${TABLE}.default_fac_only_us_departure_ports ;;
  }

  dimension: default_invoice_cancellation_disclaimer {
    type: string
    sql: ${TABLE}.default_invoice_cancellation_disclaimer ;;
  }

  dimension: default_invoice_comment {
    type: string
    sql: ${TABLE}.default_invoice_comment ;;
  }

  dimension: default_invoice_params {
    type: string
    sql: ${TABLE}.default_invoice_params ;;
  }

  dimension: default_invoice_subject {
    type: string
    sql: ${TABLE}.default_invoice_subject ;;
  }

  dimension: default_passenger_city {
    type: string
    sql: ${TABLE}.default_passenger_city ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: destination_expertise {
    type: string
    sql: ${TABLE}.destination_expertise ;;
  }

  dimension: display_name {
    type: string
    sql: ${TABLE}.display_name ;;
  }

  dimension: display_work_schedule_cp_website {
    type: yesno
    sql: ${TABLE}.display_work_schedule_cp_website ;;
  }

  dimension: display_work_schedule_website {
    type: yesno
    sql: ${TABLE}.display_work_schedule_website ;;
  }

  dimension: email_photo {
    type: string
    sql: ${TABLE}.email_photo ;;
  }

  dimension: employee_manager_id {
    type: number
    sql: ${TABLE}.employee_manager_id ;;
  }

  dimension: enable_option_expiration_notices {
    type: yesno
    sql: ${TABLE}.enable_option_expiration_notices ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
  }

  dimension: fac_count {
    type: number
    sql: ${TABLE}.fac_count ;;
  }

  dimension: facebook_fanpage_name {
    type: string
    sql: ${TABLE}.facebook_fanpage_name ;;
  }

  dimension: fat_count {
    type: number
    sql: ${TABLE}.fat_count ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: franchise_id {
    type: number
    sql: ${TABLE}.franchise_id ;;
  }

  dimension: franchise_region_id {
    type: number
    sql: ${TABLE}.franchise_region_id ;;
  }

  dimension: fri_appt_only {
    type: yesno
    sql: ${TABLE}.fri_appt_only ;;
  }

  dimension_group: fri_hours_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.fri_hours_from ;;
  }

  dimension_group: fri_hours_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.fri_hours_to ;;
  }

  dimension_group: full_sail_last_viewed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.full_sail_last_viewed ;;
  }

  dimension: gender {
    type: number
    sql: ${TABLE}.gender ;;
  }

  dimension: google_plus {
    type: string
    sql: ${TABLE}.google_plus ;;
  }

  dimension: ho_manager {
    type: yesno
    sql: ${TABLE}.ho_manager ;;
  }

  dimension: home_phone {
    type: string
    sql: ${TABLE}.home_phone ;;
  }

  dimension: hub_auto_archive {
    type: yesno
    sql: ${TABLE}.hub_auto_archive ;;
  }

  dimension: hub_auto_archive_opportunities {
    type: yesno
    sql: ${TABLE}.hub_auto_archive_opportunities ;;
  }

  dimension: iata_prin {
    type: string
    sql: ${TABLE}.iata_prin ;;
  }

  dimension_group: imagine_certified_dwd_program_certification {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.imagine_certified_dwd_program_certification_date ;;
  }

  dimension: imagine_certified_dwd_program_form_data {
    type: string
    sql: ${TABLE}.imagine_certified_dwd_program_form_data ;;
  }

  dimension_group: imagine_certified_dwd_submission {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.imagine_certified_dwd_submission_date ;;
  }

  dimension: include_all_franchise_agent_events {
    type: yesno
    sql: ${TABLE}.include_all_franchise_agent_events ;;
  }

  dimension_group: industry_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.industry_start ;;
  }

  dimension: instagram_feed {
    type: string
    sql: ${TABLE}.instagram_feed ;;
  }

  dimension: it_avatar {
    type: string
    sql: ${TABLE}.it_avatar ;;
  }

  dimension: job_status {
    type: string
    sql: ${TABLE}.job_status ;;
  }

  dimension: job_time_zone {
    type: string
    sql: ${TABLE}.job_time_zone ;;
  }

  dimension: languages {
    type: string
    sql: ${TABLE}.languages ;;
  }

  dimension_group: last_fac_ran {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_fac_ran ;;
  }

  dimension_group: last_fat_ran {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_fat_ran ;;
  }

  dimension_group: last_login {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_login ;;
  }

  dimension_group: last_mobile_sync {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_mobile_sync ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: lead_autoresponder {
    type: string
    sql: ${TABLE}.lead_autoresponder ;;
  }

  dimension: lead_autoresponder_subject {
    type: string
    sql: ${TABLE}.lead_autoresponder_subject ;;
  }

  dimension: license {
    type: string
    sql: ${TABLE}.license ;;
  }

  dimension: lifestyle {
    type: string
    sql: ${TABLE}.lifestyle ;;
  }

  dimension: linkedin_profile {
    type: string
    sql: ${TABLE}.linkedin_profile ;;
  }

  dimension: login_count {
    type: number
    sql: ${TABLE}.login_count ;;
  }

  dimension: login_token {
    type: string
    sql: ${TABLE}.login_token ;;
  }

  dimension: market_niche {
    type: string
    sql: ${TABLE}.market_niche ;;
  }

  dimension: merge_recipient_agent_id {
    type: number
    sql: ${TABLE}.merge_recipient_agent_id ;;
  }

  dimension: military_affiliation {
    type: string
    sql: ${TABLE}.military_affiliation ;;
  }

  dimension: mindflash_id {
    type: number
    sql: ${TABLE}.mindflash_id ;;
  }

  dimension: mobile_calendar_dates_sync {
    type: yesno
    sql: ${TABLE}.mobile_calendar_dates_sync ;;
  }

  dimension: mobile_chat_dnd {
    type: yesno
    sql: ${TABLE}.mobile_chat_dnd ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension: mon_appt_only {
    type: yesno
    sql: ${TABLE}.mon_appt_only ;;
  }

  dimension_group: mon_hours_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.mon_hours_from ;;
  }

  dimension_group: mon_hours_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.mon_hours_to ;;
  }

  dimension: must_update {
    type: yesno
    sql: ${TABLE}.must_update ;;
  }

  dimension: nickname {
    type: string
    sql: ${TABLE}.nickname ;;
  }

  dimension: other_name {
    type: string
    sql: ${TABLE}.other_name ;;
  }

  dimension: owner {
    type: yesno
    sql: ${TABLE}.owner ;;
  }

  dimension: owner_signer {
    type: yesno
    sql: ${TABLE}.owner_signer ;;
  }

  dimension: owner_silent {
    type: yesno
    sql: ${TABLE}.owner_silent ;;
  }

  dimension: passport {
    type: string
    sql: ${TABLE}.passport ;;
  }

  dimension_group: password_reset {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.password_reset ;;
  }

  dimension: pinterest_board {
    type: string
    sql: ${TABLE}.pinterest_board ;;
  }

  dimension: previous_occupation {
    type: string
    sql: ${TABLE}.previous_occupation ;;
  }

  dimension: primary {
    type: yesno
    sql: ${TABLE}."primary" ;;
  }

  dimension: protege {
    type: yesno
    sql: ${TABLE}.protege ;;
  }

  dimension: protege_franchise {
    type: yesno
    sql: ${TABLE}.protege_franchise ;;
  }

  dimension: public_safety_affiliation {
    type: string
    sql: ${TABLE}.public_safety_affiliation ;;
  }

  dimension: race {
    type: string
    sql: ${TABLE}.race ;;
  }

  dimension: remove_zero_balance_scheduled_payments {
    type: yesno
    sql: ${TABLE}.remove_zero_balance_scheduled_payments ;;
  }

  dimension: sat_appt_only {
    type: yesno
    sql: ${TABLE}.sat_appt_only ;;
  }

  dimension_group: sat_hours_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sat_hours_from ;;
  }

  dimension_group: sat_hours_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sat_hours_to ;;
  }

  dimension: send_carnival_upgrades {
    type: yesno
    sql: ${TABLE}.send_carnival_upgrades ;;
  }

  dimension: show_commission_on_quick_quote {
    type: yesno
    sql: ${TABLE}.show_commission_on_quick_quote ;;
  }

  dimension: soft_date_days {
    type: number
    sql: ${TABLE}.soft_date_days ;;
  }

  dimension: ssn {
    type: string
    sql: ${TABLE}.ssn ;;
  }

  dimension: star_level_id {
    type: number
    sql: ${TABLE}.star_level_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension: sun_appt_only {
    type: yesno
    sql: ${TABLE}.sun_appt_only ;;
  }

  dimension_group: sun_hours_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sun_hours_from ;;
  }

  dimension_group: sun_hours_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sun_hours_to ;;
  }

  dimension: terminated {
    type: yesno
    sql: ${TABLE}.terminated ;;
  }

  dimension: thu_appt_only {
    type: yesno
    sql: ${TABLE}.thu_appt_only ;;
  }

  dimension_group: thu_hours_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.thu_hours_from ;;
  }

  dimension_group: thu_hours_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.thu_hours_to ;;
  }

  dimension: ticket_system_admin {
    type: number
    sql: ${TABLE}.ticket_system_admin ;;
  }

  dimension: toll_free_phone {
    type: string
    sql: ${TABLE}.toll_free_phone ;;
  }

  dimension: tou_agree {
    type: yesno
    sql: ${TABLE}.tou_agree ;;
  }

  dimension_group: training {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.training_date ;;
  }

  dimension: training_exempt {
    type: yesno
    sql: ${TABLE}.training_exempt ;;
  }

  dimension: training_status {
    type: string
    sql: ${TABLE}.training_status ;;
  }

  dimension: training_type {
    type: string
    sql: ${TABLE}.training_type ;;
  }

  dimension: trip_summary_password {
    type: string
    sql: ${TABLE}.trip_summary_password ;;
  }

  dimension: trip_summary_username {
    type: string
    sql: ${TABLE}.trip_summary_username ;;
  }

  dimension: tue_appt_only {
    type: yesno
    sql: ${TABLE}.tue_appt_only ;;
  }

  dimension_group: tue_hours_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.tue_hours_from ;;
  }

  dimension_group: tue_hours_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.tue_hours_to ;;
  }

  dimension: twitter_username {
    type: string
    sql: ${TABLE}.twitter_username ;;
  }

  dimension: upcoming_hard_payment_dates {
    type: yesno
    sql: ${TABLE}.upcoming_hard_payment_dates ;;
  }

  dimension: upcoming_soft_payment_dates {
    type: yesno
    sql: ${TABLE}.upcoming_soft_payment_dates ;;
  }

  dimension: use_lead_autoresponder {
    type: yesno
    sql: ${TABLE}.use_lead_autoresponder ;;
  }

  dimension: use_reshelper {
    type: yesno
    sql: ${TABLE}.use_reshelper ;;
  }

  dimension: vanity_phone {
    type: string
    sql: ${TABLE}.vanity_phone ;;
  }

  dimension_group: viewed_snapshot {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.viewed_snapshot ;;
  }

  dimension: wed_appt_only {
    type: yesno
    sql: ${TABLE}.wed_appt_only ;;
  }

  dimension_group: wed_hours_from {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wed_hours_from ;;
  }

  dimension_group: wed_hours_to {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wed_hours_to ;;
  }

  dimension: work_phone {
    type: string
    sql: ${TABLE}.work_phone ;;
  }

  dimension: youtube_channel {
    type: string
    sql: ${TABLE}.youtube_channel ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      twitter_username,
      trip_summary_username,
      other_name,
      nickname,
      last_name,
      first_name,
      facebook_fanpage_name,
      display_name,
      reservations.count
    ]
  }
}
