view: franchise_call_last2last_mill {
# view: franchise_call_number_last_to_last {
  derived_table: {
    sql:

                {% if _user_attributes['role'] == 'Manager' %}

    SELECT f.franchise_region_id AS "region_id"
              ,f.id AS f_id
              ,f.star_level_id
              ,f.display_name
              --,temp.call_entry_year
              --,temp.call_entry_quarter
              --,temp.call_entry_month
              ,COALESCE(temp.score,'Never') AS "times_called"
              ,case temp.Month
                  when 1 then 'January'
                  when 2 then 'February'
                  when 3 then 'March'
                  when 4 then 'April'
                  when 5 then 'May'
                  when 6 then 'June'
                  when 7 then 'July'
                  when 8 then 'August'
                  when 9 then 'September'
                  when 10 then 'October'
                  when 11 then 'November'
                  else 'December'
                  end AS "Month"

        FROM excolo.franchises f
        LEFT JOIN(
                SELECT f.id
                      ,EXTRACT(MONTH FROM cte.created) as "Month"
                      -- ,CASE WHEN COUNT(cte.id)>=2 THEN 'Atleast Twice' ELSE 'Once' END AS score
                      ,CASE WHEN COUNT(cte.id)>=1 THEN 'Atleast Once' ELSE 'Zero' END AS score


                FROM excolo.call_tracker_entries cte
                INNER JOIN excolo.agents a ON a.id=cte.agent_id
                      AND a.deleted=0 -- and a.terminated=0
                INNER JOIN excolo.franchises f ON f.id=a.franchise_id
                      AND f.deleted=0
                      AND f.franchise_status_id IN (1,2)
                      AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000


                WHERE --{% condition call_year %}cte.created{% endcondition %}
                      --(
                      ({% condition call_year %} EXTRACT(YEAR FROM cte.created) {% endcondition %}
                      AND {% condition call_quarter %} EXTRACT(QUARTER FROM cte.created) {% endcondition %})
                      --OR {% condition call_year %}cte.created{% endcondition %})
                      --=2018 -- EXTRACT(YEAR FROM CURRENT_DATE)
                      -- AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
                       AND cte.call_type IN ('outbound_coaching')
                       AND cte.left_voicemail = 0
                       AND ((cte.duration != '< 5 mins' AND f.star_level_id != 2) OR (f.star_level_id = 2))
                       AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)

                GROUP BY f.id, EXTRACT(MONTH FROM cte.created)

                having EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))
                                                          from excolo.call_tracker_entries cte
                                                          where --{% condition call_year %}cte.created{% endcondition %}
                                                                --(
                                                                ({% condition call_year %} EXTRACT(YEAR FROM cte.created) {% endcondition %}
                                                                AND {% condition call_quarter %} EXTRACT(QUARTER FROM cte.created) {% endcondition %})
                                                                --OR {% condition call_year %}cte.created{% endcondition %})
                                                          )

                -- order by f.id, EXTRACT(MONTH FROM cte.created)
        )temp ON temp.id = f.id

        WHERE f.deleted=0
              AND f.franchise_status_id IN (1,2)
              AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000
              AND f.franchise_region_id IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
             --  AND ((EXTRACT(year from f.start_date) = 2018 AND EXTRACT(month from f.start_date) <= 7)
              --    OR EXTRACT(year from f.start_date) < 2018)


               -- AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}

                 {% else %}
                     SELECT f.franchise_region_id AS "region_id"
       ,f.id AS f_id
      ,f.star_level_id
       ,f.display_name
       ,COALESCE(temp.score,'Never') AS "times_called"
       ,case EXTRACT(MONTH FROM CURRENT_DATE) - 2
             when 1 then 'January'
             when 2 then 'February'
             when 3 then 'March'
             when 4 then 'April'
             when 5 then 'May'
             when 6 then 'June'
             when 7 then 'July'
             when 8 then 'August'
             when 9 then 'September'
             when 10 then 'October'
             when 11 then 'November'
             else 'December'
             end AS "Month"
       FROM excolo.franchises f
       LEFT JOIN
       ( SELECT f.id
                      -- ,CASE WHEN COUNT(cte.id)>=2 THEN 'Atleast Twice' ELSE 'Once' END AS score
                      ,CASE WHEN COUNT(cte.id)>=1 THEN 'Atleast Once' ELSE 'Zero' END AS score
        FROM excolo.call_tracker_entries cte
        INNER JOIN excolo.agents a ON a.id=cte.agent_id AND a.deleted=0 -- and a.terminated=0
        INNER JOIN excolo.franchises f ON f.id=a.franchise_id AND f.deleted=0 AND f.franchise_status_id IN (1,2) AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000
        WHERE EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
        AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
        AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 2
        AND cte.call_type IN ('outbound_coaching')
                       AND cte.left_voicemail = 0
                       AND ((cte.duration != '< 5 mins' AND f.star_level_id != 2) OR (f.star_level_id = 2))
                       AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
        GROUP BY f.id

       )temp ON temp.id = f.id
       WHERE f.deleted=0 AND f.franchise_status_id IN (1,2) AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000 AND f.franchise_region_id IN (8,9,10,11,12,14,15,16)
       -- ORDER BY f.franchise_region_id
                  {% endif %}


    -- ORDER BY f.franchise_region_id

                               ;;
    #persist_for: "5 minutes" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours
    #distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

    }
    # FILTERS #

    filter: call_year {
      type: string
    }
    filter: call_quarter {
      type: string
    }

    # DIMENSIONS #

#   dimension: call_year {
#     type: number
#     sql: Extract year from (${call_date}) ;;
#   }
#
#   dimension: call_quarter {
#     type: number
#     sql: Extract quarter from (${call_date}) ;;
#   }

    dimension: franchise_id {
      primary_key: yes
      type: number
      sql: ${TABLE}.f_id ;;
    }

    dimension: star_level_id {
      type: number
      sql: ${TABLE}.star_level_id ;;
    }

    dimension: Region_id{
      type: number
      sql: ${TABLE}.region_id ;;
    }

    dimension: times_called {
      type: string
      sql: ${TABLE}.times_called ;;
    }

    dimension: Month {
      type: string
      sql: ${TABLE}.Month ;;
    }


  }
