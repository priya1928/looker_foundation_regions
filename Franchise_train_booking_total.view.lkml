view: Franchise_train_booking_total {
  derived_table: {
    sql:SELECT COUNT(DISTINCT r.id) AS Total_booking_count

                                ,f.id AS franchise_id

                                ,f.star_level_id

                                ,f.training_date AS Training_Date

                                ,f.franchise_region_id AS Region

                                ,EXTRACT(YEAR FROM f.training_date) AS Training_Year -- YEAR(f.training_date)

                                ,EXTRACT(MONTH FROM f.training_date) AS Training_Month

                                ,CONCAT(f.id,CONCAT(f.franchise_region_id,CONCAT(EXTRACT(YEAR FROM f.training_date),EXTRACT(MONTH FROM f.training_date)))) AS linking

                                FROM excolo.agents a

                                                INNER JOIN excolo.reservations r

                                                ON r.agent_id = a.id



                                INNER JOIN excolo.franchises f

                                                ON a.franchise_id=f.id AND f.deleted=0


                                WHERE r.deleted=0 AND a.deleted=0 AND a.terminated=0 AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000
                                AND f.franchise_status_id IN (1,2)
                                AND f.franchise_region_id IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
                                GROUP BY Training_Year, Training_Month, f.training_date, f.franchise_region_id,f.id,f.star_level_id
                                ORDER BY Training_Year DESC
       ;;
 #  persist_for: "35 minutes" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours
 # distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

  }
  # DIMENSIONS #
  dimension: Total_booking_count {
    type: number
    sql: ${TABLE}. Total_booking_count ;;
  }

  dimension: franchise_id {
    type: number
    primary_key: yes
    sql: ${TABLE}.franchise_id ;;
  }

  dimension: star_level_id {
    type: number
    sql: ${TABLE}.star_level_id ;;
  }

  dimension: Training_Date {
    type: date
    sql: ${TABLE}.Training_Date ;;
  }

  dimension: Training_year {
    type: number
    sql: extract(year from ${Training_Date}) ;;
  }

  dimension: Training_quarter {
    type: number
    sql: extract(quarter from ${Training_Date}) ;;
  }

  dimension: Training_Class {
    type: string
    sql: CONCAT(${TABLE}.Training_Month,CONCAT('/',${TABLE}.Training_Year)) ;;
  }

  dimension: Franchise_region_id {
    type: number
    sql: ${TABLE}.Region ;;
  }

  dimension: Total_linking {
    type: string
    sql: ${TABLE}.linking ;;
  }

  measure: sum_Total_booking {
    type: sum
    sql:  ${TABLE}.Total_booking_count ;;
    drill_fields: [franchise_detail*]
  }

  set: franchise_detail {
    fields: [
      franchises.contract_id,
      franchise_owner_information.Owner_name,
      star_levels.Star_Level,
      franchise_owner_information.Work_phone,
      franchise_owner_information.Training_date,
      franchise_owner_information.last_coaching_call,
      franchise_sales.Total_sales

    ]
  }

}
