view: franchises_non_producing {
  derived_table: {
    sql:SELECT f.id AS f_id,
f.franchise_region_id AS "region_id",
MAX(COALESCE(r.agent_id,0)) AS "State"

FROM excolo.franchises f

INNER JOIN excolo.agents a ON a.franchise_id=f.id AND a.deleted=0 AND a.terminated=0

LEFT JOIN excolo.reservations r ON r.agent_id=a.id AND r.deleted=0

WHERE f.deleted=0 AND f.franchise_status_id IN (1,2) AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000
AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14)
GROUP BY f.id, f.franchise_region_id
HAVING MAX(COALESCE(r.agent_id,0)) = 0

ORDER BY  f.franchise_region_id, f.id

             ;;
   # persist_for: "35 minutes" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours
   # distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

  }
  # DIMENSIONS #
  dimension: franchise_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.f_id ;;
  }

  dimension:region_id{
    type: number
    sql: ${TABLE}.region_id ;;
  }

}
