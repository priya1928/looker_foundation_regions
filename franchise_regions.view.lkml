view: franchise_regions {
  sql_table_name: excolo.franchise_regions ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: name {
    type: string
    sql: ${TABLE}.name ;;
  }

  dimension: region_with_parent {
    type: string
    sql: case
     when (${id} = 1 or ${id} = 11) then 'Foundation - Central'
     when (${id} = 2 or ${id} = 8) then 'Foundation - Northeast'
     when (${id} = 3 or ${id} = 12) then 'Foundation - South Florida'
     when (${id} = 4 or ${id} = 9) then 'Foundation - Southeast'
     when (${id} = 5 or ${id} = 10) then 'Foundation - West'
     when (${id} = 13 or ${id} = 14) then 'Foundation - Midwest'
     when (${id} = 15 or ${id} = 15) then 'Transition - East'
     when (${id} = 16 or ${id} = 16) then 'Transition - West'
              else 'Any'
              end ;;
  }

  dimension: sort_order {
    type: number
    sql: ${TABLE}.sort_order ;;
  }

  measure: count {
    type: count
    drill_fields: [id, name]
  }
}
