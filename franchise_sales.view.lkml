view: franchise_sales {
  derived_table: {
    sql: select f.id AS "franchise_id"
,sum(rmd.total_sales) AS "Total_sales"
,sum(rmd.commissionable_sales) AS "Commissionable_sales"
from excolo.reservation_meta_data rmd
INNER JOIN excolo.franchises f ON rmd.franchise_id = f.id
                        AND f.deleted=0
                        AND cast(coalesce(nullif(f.contract_id,''), '0') as integer) >=8000
                        AND f.franchise_status_id IN (1,2)
                        AND rmd.deleted = 0
 group by f.id
 order by f.id
;;
    #persist_for: "1 hours" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours
    #distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

  }
  # DIMENSIONS #
  dimension: franchise_id {
    type: number
    primary_key: yes
    sql: ${TABLE}.franchise_id ;;
  }

  dimension: Total_sales {
    type: number
    sql: ${TABLE}.Total_sales ;;
  }

  dimension: Commissionable_sales {
    type: number
    sql: ${TABLE}.Commissionable_sales ;;
  }

  measure: count {
    type: count
    drill_fields: [franchise_detail*]
  }

  # ----- Sets of fields for drilling ------
  set: franchise_detail {
    fields: [
      franchises.contract_id,
      franchise_owner_information.Owner_name,
      franchise_owner_information.Work_phone,
      franchise_owner_information.Training_date,
      franchise_owner_information.last_coaching_call,
      franchise_meta_data.departures_year_to_date_sales
    ]
  }


}
