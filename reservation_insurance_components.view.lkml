view: reservation_insurance_components {
  sql_table_name: excolo.reservation_insurance_components ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: _sdc_batched {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_batched_at ;;
  }

  dimension_group: _sdc_extracted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_extracted_at ;;
  }

  dimension_group: _sdc_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_received_at ;;
  }

  dimension: _sdc_sequence {
    type: number
    sql: ${TABLE}._sdc_sequence ;;
  }

  dimension: _sdc_table_version {
    type: number
    sql: ${TABLE}._sdc_table_version ;;
  }

  dimension: address1 {
    type: string
    sql: ${TABLE}.address1 ;;
  }

  dimension: address2 {
    type: string
    sql: ${TABLE}.address2 ;;
  }

  dimension: booking_source {
    type: number
    sql: ${TABLE}.booking_source ;;
  }

  dimension: city {
    type: string
    sql: ${TABLE}.city ;;
  }

  dimension: country_id {
    type: number
    sql: ${TABLE}.country_id ;;
  }

  dimension: cp_commission {
    type: number
    sql: ${TABLE}.cp_commission ;;
  }

  dimension: cp_percent_amount {
    type: number
    sql: ${TABLE}.cp_percent_amount ;;
  }

  dimension: cp_percent_commission {
    type: number
    sql: ${TABLE}.cp_percent_commission ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: destination_country_id {
    type: number
    sql: ${TABLE}.destination_country_id ;;
  }

  dimension_group: end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.end_date ;;
  }

  dimension_group: ho_locked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ho_locked ;;
  }

  dimension: insurance_company_id {
    type: number
    sql: ${TABLE}.insurance_company_id ;;
  }

  dimension: insurance_company_product_id {
    type: number
    sql: ${TABLE}.insurance_company_product_id ;;
  }

  dimension_group: locked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.locked ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension: mytrips_declined {
    type: yesno
    sql: ${TABLE}.mytrips_declined ;;
  }

  dimension: mytrips_declined_by {
    type: string
    sql: ${TABLE}.mytrips_declined_by ;;
  }

  dimension_group: mytrips_declined {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.mytrips_declined_date ;;
  }

  dimension: mytrips_purchasable {
    type: yesno
    sql: ${TABLE}.mytrips_purchasable ;;
  }

  dimension: mytrips_purchased {
    type: yesno
    sql: ${TABLE}.mytrips_purchased ;;
  }

  dimension: mytrips_purchased_by {
    type: string
    sql: ${TABLE}.mytrips_purchased_by ;;
  }

  dimension: notes {
    type: string
    sql: ${TABLE}.notes ;;
  }

  dimension: on_quote {
    type: yesno
    sql: ${TABLE}.on_quote ;;
  }

  dimension: origination_country_id {
    type: number
    sql: ${TABLE}.origination_country_id ;;
  }

  dimension: policy_number {
    type: string
    sql: ${TABLE}.policy_number ;;
  }

  dimension: pricing_only {
    type: yesno
    sql: ${TABLE}.pricing_only ;;
  }

  dimension_group: purchase {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.purchase_date ;;
  }

  dimension: reservation_cruise_component_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.reservation_cruise_component_id ;;
  }

  dimension: reservation_id {
    type: number
    sql: ${TABLE}.reservation_id ;;
  }

  dimension: reservation_tour_component_id {
    type: number
    sql: ${TABLE}.reservation_tour_component_id ;;
  }

  dimension: reservation_vendor_id {
    type: number
    sql: ${TABLE}.reservation_vendor_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension: state_id {
    type: number
    sql: ${TABLE}.state_id ;;
  }

  dimension: switched_to_manual {
    type: yesno
    sql: ${TABLE}.switched_to_manual ;;
  }

  dimension: transactionid {
    type: string
    sql: ${TABLE}.transactionid ;;
  }

  dimension: trip_cost {
    type: number
    sql: ${TABLE}.trip_cost ;;
  }

  dimension: zip {
    type: zipcode
    sql: ${TABLE}.zip ;;
  }

  measure: count {
    type: count
    drill_fields: [id, reservation_cruise_components.id, reservation_cruise_components.rate_option_nr_name, reservation_cruise_components.group_name, reservation_cruise_components.category_name]
  }
}
