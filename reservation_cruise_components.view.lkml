view: reservation_cruise_components {
  sql_table_name: excolo.reservation_cruise_components ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: _sdc_batched {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_batched_at ;;
  }

  dimension_group: _sdc_extracted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_extracted_at ;;
  }

  dimension_group: _sdc_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_received_at ;;
  }

  dimension: _sdc_sequence {
    type: number
    sql: ${TABLE}._sdc_sequence ;;
  }

  dimension: _sdc_table_version {
    type: number
    sql: ${TABLE}._sdc_table_version ;;
  }

  dimension: amex_cruise_group {
    type: yesno
    sql: ${TABLE}.amex_cruise_group ;;
  }

  dimension: amex_cruise_group_number {
    type: string
    sql: ${TABLE}.amex_cruise_group_number ;;
  }

  dimension: amex_cuba {
    type: yesno
    sql: ${TABLE}.amex_cuba ;;
  }

  dimension: amex_mariner_club {
    type: yesno
    sql: ${TABLE}.amex_mariner_club ;;
  }

  dimension: amex_program_id {
    type: number
    sql: ${TABLE}.amex_program_id ;;
  }

  dimension_group: api_option {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.api_option_date ;;
  }

  dimension: api_option_deposit_amount {
    type: number
    sql: ${TABLE}.api_option_deposit_amount ;;
  }

  dimension: booking_source {
    type: number
    sql: ${TABLE}.booking_source ;;
  }

  dimension: cabin_number {
    type: string
    sql: ${TABLE}.cabin_number ;;
  }

  dimension: category_name {
    type: string
    sql: ${TABLE}.category_name ;;
  }

  dimension: confirmed_seating {
    type: number
    sql: ${TABLE}.confirmed_seating ;;
  }

  dimension: consumer_booking {
    type: yesno
    sql: ${TABLE}.consumer_booking ;;
  }

  dimension: cp_commission {
    type: number
    sql: ${TABLE}.cp_commission ;;
  }

  dimension: cp_percent_amount {
    type: number
    sql: ${TABLE}.cp_percent_amount ;;
  }

  dimension: cp_percent_commission {
    type: number
    sql: ${TABLE}.cp_percent_commission ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: cruise_sheet_booking {
    type: yesno
    sql: ${TABLE}.cruise_sheet_booking ;;
  }

  dimension: currency_id {
    type: number
    sql: ${TABLE}.currency_id ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: departure_city_id {
    type: number
    sql: ${TABLE}.departure_city_id ;;
  }

  dimension: econnect_soft_payment_date_modified {
    type: yesno
    sql: ${TABLE}.econnect_soft_payment_date_modified ;;
  }

  dimension: excluded_from_trip_summary {
    type: yesno
    sql: ${TABLE}.excluded_from_trip_summary ;;
  }

  dimension: final_payment_balance {
    type: number
    sql: ${TABLE}.final_payment_balance ;;
  }

  dimension_group: final_payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.final_payment_date ;;
  }

  dimension: group_econnect_link {
    type: yesno
    sql: ${TABLE}.group_econnect_link ;;
  }

  dimension: group_id {
    type: number
    sql: ${TABLE}.group_id ;;
  }

  dimension: group_name {
    type: string
    sql: ${TABLE}.group_name ;;
  }

  dimension: group_number {
    type: string
    sql: ${TABLE}.group_number ;;
  }

  dimension: has_tc {
    type: yesno
    sql: ${TABLE}.has_tc ;;
  }

  dimension_group: ho_locked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ho_locked ;;
  }

  dimension: hold {
    type: yesno
    sql: ${TABLE}.hold ;;
  }

  dimension: hold_agent_id {
    type: number
    sql: ${TABLE}.hold_agent_id ;;
  }

  dimension_group: hold_set {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.hold_set_date ;;
  }

  dimension: itinerary_id {
    type: number
    sql: ${TABLE}.itinerary_id ;;
  }

  dimension_group: locked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.locked ;;
  }

  dimension: mobile_app_booking {
    type: yesno
    sql: ${TABLE}.mobile_app_booking ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension_group: option {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.option_date ;;
  }

  dimension: package_id {
    type: number
    sql: ${TABLE}.package_id ;;
  }

  dimension: pricing_only {
    type: yesno
    sql: ${TABLE}.pricing_only ;;
  }

  dimension: private_group_id {
    type: number
    sql: ${TABLE}.private_group_id ;;
  }

  dimension_group: purchase {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.purchase_date ;;
  }

  dimension: rate_option_code {
    type: string
    sql: ${TABLE}.rate_option_code ;;
  }

  dimension: rate_option_nr_name {
    type: string
    sql: ${TABLE}.rate_option_nr_name ;;
  }

  dimension: rate_option_type {
    type: string
    sql: ${TABLE}.rate_option_type ;;
  }

  dimension: res_number {
    type: string
    sql: ${TABLE}.res_number ;;
  }

  dimension: reservation_id {
    type: number
    sql: ${TABLE}.reservation_id ;;
  }

  dimension: reservation_vendor_id {
    type: number
    sql: ${TABLE}.reservation_vendor_id ;;
  }

  dimension: return_city_id {
    type: number
    sql: ${TABLE}.return_city_id ;;
  }

  dimension_group: return {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.return_date ;;
  }

  dimension_group: sailing {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sailing_date ;;
  }

  dimension: seating {
    type: number
    sql: ${TABLE}.seating ;;
  }

  dimension: ship_category_id {
    type: number
    sql: ${TABLE}.ship_category_id ;;
  }

  dimension: ship_id {
    type: number
    sql: ${TABLE}.ship_id ;;
  }

  dimension: smoking {
    type: yesno
    sql: ${TABLE}.smoking ;;
  }

  dimension_group: soft_payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.soft_payment_date ;;
  }

  dimension: subregion_id {
    type: number
    sql: ${TABLE}.subregion_id ;;
  }

  dimension: switched_to_manual {
    type: yesno
    sql: ${TABLE}.switched_to_manual ;;
  }

  dimension: table_size {
    type: string
    sql: ${TABLE}.table_size ;;
  }

  dimension: tc_display_type {
    type: number
    sql: ${TABLE}.tc_display_type ;;
  }

  dimension: unscheduled_balance {
    type: number
    sql: ${TABLE}.unscheduled_balance ;;
  }

  dimension: upgrade_from_category_code {
    type: string
    sql: ${TABLE}.upgrade_from_category_code ;;
  }

  dimension: upgrade_from_ship_category_id {
    type: number
    sql: ${TABLE}.upgrade_from_ship_category_id ;;
  }

  dimension: waitlisted {
    type: yesno
    sql: ${TABLE}.waitlisted ;;
  }

  measure: count {
    type: count
    drill_fields: [id, rate_option_nr_name, group_name, category_name, reservation_insurance_components.count]
  }
}
