view: reservation_meta_data {
  sql_table_name: excolo.reservation_meta_data ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: _sdc_batched {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_batched_at ;;
  }

  dimension_group: _sdc_extracted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_extracted_at ;;
  }

  dimension_group: _sdc_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_received_at ;;
  }

  dimension: _sdc_sequence {
    type: number
    sql: ${TABLE}._sdc_sequence ;;
  }

  dimension: _sdc_table_version {
    type: number
    sql: ${TABLE}._sdc_table_version ;;
  }

  dimension: agent_id {
    type: number
    sql: ${TABLE}.agent_id ;;
  }

  dimension: commissionable_sales {
    type: number
    sql: ${TABLE}.commissionable_sales ;;
  }

  dimension: confirmation_number {
    type: string
    sql: ${TABLE}.confirmation_number ;;
  }

  dimension: cp_commission {
    type: number
    sql: ${TABLE}.cp_commission ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension_group: departure {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.departure_date ;;
  }

  dimension_group: final_payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.final_payment_date ;;
  }

  dimension_group: final_soft_payment {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.final_soft_payment_date ;;
  }

  dimension: franchise_commission {
    type: number
    sql: ${TABLE}.franchise_commission ;;
  }

  dimension: franchise_commission_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.franchise_commission_paid ;;
  }

  dimension: franchise_id {
    type: number
    sql: ${TABLE}.franchise_id ;;
  }

  dimension: gross_commissionable_sales {
    type: number
    sql: ${TABLE}.gross_commissionable_sales ;;
  }

  dimension: gross_total_sales {
    type: number
    sql: ${TABLE}.gross_total_sales ;;
  }

  dimension: gross_vendor_commission {
    type: number
    sql: ${TABLE}.gross_vendor_commission ;;
  }

  dimension: group_id {
    type: number
    sql: ${TABLE}.group_id ;;
  }

  dimension: group_number {
    type: string
    sql: ${TABLE}.group_number ;;
  }

  dimension: group_source {
    type: string
    sql: ${TABLE}.group_source ;;
  }

  dimension: group_type {
    type: string
    sql: ${TABLE}.group_type ;;
  }

  dimension: has_amex_payment {
    type: yesno
    sql: ${TABLE}.has_amex_payment ;;
  }

  dimension: incentive_commissionable_sales {
    type: number
    sql: ${TABLE}.incentive_commissionable_sales ;;
  }

  dimension: incentive_insurance_commissionable_sales {
    type: number
    sql: ${TABLE}.incentive_insurance_commissionable_sales ;;
  }

  dimension: incentive_insurance_total_sales {
    type: number
    sql: ${TABLE}.incentive_insurance_total_sales ;;
  }

  dimension: incentive_total_sales {
    type: number
    sql: ${TABLE}.incentive_total_sales ;;
  }

  dimension: itinerary_id {
    type: number
    sql: ${TABLE}.itinerary_id ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension: on_quote {
    type: yesno
    sql: ${TABLE}.on_quote ;;
  }

  dimension: passenger_count {
    type: number
    sql: ${TABLE}.passenger_count ;;
  }

  dimension: posted_payment_amount {
    type: number
    sql: ${TABLE}.posted_payment_amount ;;
  }

  dimension: posted_payment_balance {
    type: number
    sql: ${TABLE}.posted_payment_balance ;;
  }

  dimension: posted_refund_amount {
    type: number
    sql: ${TABLE}.posted_refund_amount ;;
  }

  dimension: primary_client_id {
    type: number
    sql: ${TABLE}.primary_client_id ;;
  }

  dimension: primary_reservation_vendor_id {
    type: number
    sql: ${TABLE}.primary_reservation_vendor_id ;;
  }

  dimension: primary_vendor_id {
    type: number
    sql: ${TABLE}.primary_vendor_id ;;
  }

  dimension: private_group_id {
    type: number
    sql: ${TABLE}.private_group_id ;;
  }

  dimension_group: purchase {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.purchase_date ;;
  }

  dimension: region_id {
    type: number
    sql: ${TABLE}.region_id ;;
  }

  dimension: reservation_id {
    type: number
    sql: ${TABLE}.reservation_id ;;
  }

  dimension_group: return {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.return_date ;;
  }

  dimension: ship_category_id {
    type: number
    sql: ${TABLE}.ship_category_id ;;
  }

  dimension: ship_id {
    type: number
    sql: ${TABLE}.ship_id ;;
  }

  dimension: subregion_id {
    type: number
    sql: ${TABLE}.subregion_id ;;
  }

  dimension: total_additional {
    type: number
    sql: ${TABLE}.total_additional ;;
  }

  dimension: total_commission {
    type: number
    sql: ${TABLE}.total_commission ;;
  }

  dimension: total_coupons {
    type: number
    sql: ${TABLE}.total_coupons ;;
  }

  dimension: total_discounts {
    type: number
    sql: ${TABLE}.total_discounts ;;
  }

  dimension: total_markup {
    type: number
    sql: ${TABLE}.total_markup ;;
  }

  dimension: total_sales {
    type: number
    sql: ${TABLE}.total_sales ;;
  }

  dimension: vendor_commission {
    type: number
    sql: ${TABLE}.vendor_commission ;;
  }

  dimension: vendor_commission_paid {
    type: number
    value_format_name: id
    sql: ${TABLE}.vendor_commission_paid ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
