view: coach_foundation_third_month_call {
    derived_table: {
      sql:

                {% if _user_attributes['role'] == 'Manager' %}

           SELECT f.franchise_region_id AS "region_id"
              ,f.id AS f_id
              ,f.star_level_id
              ,f.display_name
              --,temp.call_entry_year
              --,temp.call_entry_quarter
              --,temp.call_entry_month
              ,temp.total_cc
              ,temp.total_calls
              ,COALESCE(temp.all_calls,0) as "atleast_2_calls"
              ,COALESCE(temp.TCC,0) as "atleast_2_coaching_calls"
              ,COALESCE(temp.OCC,0) as "occ"
              ,COALESCE(temp.NCC,1) as "ncc"
              ,CASE
                  WHEN temp.id IS NULL
                  THEN 1
                  ELSE 0
                END as "never_called"
             -- ,COALESCE(temp.score,'Never') AS "times_called"
              ,case temp.Month
                  when 1 then 'January'
                  when 2 then 'February'
                  when 3 then 'March'
                  when 4 then 'April'
                  when 5 then 'May'
                  when 6 then 'June'
                  when 7 then 'July'
                  when 8 then 'August'
                  when 9 then 'September'
                  when 10 then 'October'
                  when 11 then 'November'
                  else 'December'
                  end AS "Month"

        FROM excolo.franchises f
        LEFT JOIN(
                SELECT f.id
                      ,EXTRACT(MONTH FROM cte.created) as "Month"
                      ,count(cte.id) as "total_calls"
                      ,sum(
                            CASE
                                  WHEN cte.duration != '< 5 mins' THEN 1
                                  ELSE 0
                       END) AS "total_cc"
                      ,CASE
                            WHEN total_calls >=2
                            THEN 1
                            ELSE 0
                        END AS "all_calls"
                      ,CASE
                            WHEN total_cc >=2
                            THEN 1
                            ELSE 0
                        END as "TCC"
                      ,CASE
                            WHEN total_cc = 1
                            THEN 1
                            ELSE 0
                        END as "OCC"
                      ,CASE
                            WHEN total_cc = 0
                            THEN 1
                            ELSE 0
                        END as "NCC"
                      -- ,CASE WHEN COUNT(cte.id)>=1 THEN 'Atleast Once' ELSE 'Zero' END AS score


                FROM excolo.call_tracker_entries cte
                INNER JOIN excolo.agents a ON a.id=cte.agent_id
                      AND a.deleted=0 -- and a.terminated=0
                INNER JOIN excolo.franchises f ON f.id=a.franchise_id
                      AND f.deleted=0
                      AND f.franchise_status_id IN (1,2)
                      AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000

                WHERE {% condition call_date %}cte.created{% endcondition %}--=2018 -- EXTRACT(YEAR FROM CURRENT_DATE)
                       -- AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
                      -- AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
                       AND cte.call_type IN ('outbound_coaching')
                      -- AND cte.left_voicemail = 0
                      -- AND ((cte.duration != '< 5 mins' AND f.star_level_id != 2) OR (f.star_level_id = 2))
                       AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)

                GROUP BY f.id, EXTRACT(MONTH FROM cte.created)

                having EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))+2
                                                          from excolo.call_tracker_entries cte
                                                          where {% condition call_date %}cte.created{% endcondition %})--=2018 -- EXTRACT(YEAR FROM CURRENT_DATE)

                order by f.id, EXTRACT(MONTH FROM cte.created)
        )temp ON temp.id = f.id

        WHERE f.deleted=0
              AND f.franchise_status_id IN (1,2)
              AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000
              AND f.franchise_region_id IN (8,9,10,11,12,14,15,16)


                 {% else %}
      SELECT f.franchise_region_id AS "region_id"
 ,f.id AS f_id
 ,f.star_level_id
 ,f.display_name
 ,COALESCE(temp.score,'Never') AS "times_called"
 ,case EXTRACT(MONTH FROM CURRENT_DATE)
       when 1 then 'January'
       when 2 then 'February'
       when 3 then 'March'
       when 4 then 'April'
       when 5 then 'May'
       when 6 then 'June'
       when 7 then 'July'
       when 8 then 'August'
       when 9 then 'September'
       when 10 then 'October'
       when 11 then 'November'
       else 'December'
       end AS "Month"
 FROM excolo.franchises f
 LEFT JOIN
 ( SELECT f.id
                      ,CASE WHEN COUNT(cte.id)>=2 THEN 'Atleast Twice' ELSE 'Once' END AS score
                      -- ,CASE WHEN COUNT(cte.id)>=1 THEN 'Atleast Once' ELSE 'Zero' END
  -- AS score
  FROM excolo.call_tracker_entries cte
  INNER JOIN excolo.agents a ON a.id=cte.agent_id AND a.deleted=0 -- and a.terminated=0
  INNER JOIN excolo.franchises f ON f.id=a.franchise_id AND f.deleted=0 AND f.franchise_status_id IN (1,2) AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000
  WHERE EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
  AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
  AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
  AND cte.call_type IN ('outbound_coaching')
  AND cte.left_voicemail = 0
  AND ((cte.duration != '< 5 mins' AND f.star_level_id != 2) OR (f.star_level_id = 2))
  AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
  GROUP BY f.id

 )temp ON temp.id = f.id
 WHERE f.deleted=0 AND f.franchise_status_id IN (1,2) AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000 AND f.franchise_region_id IN (8,9,10,11,12,14,15,16)
 -- ORDER BY f.franchise_region_id
                  {% endif %}

    -- ORDER BY f.franchise_region_id

    ;;
    #persist_for: "5 minutes" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours#
  #distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

      }

      # FILTERS #

      filter: call_date {
        type: date
      }
#
#   filter: call_quarter {
#     type: number
#   }
#
#   filter: call_month {
#     type: number
#   }

      # DIMENSIONS #

      dimension: franchise_id {
        primary_key: yes
        type: number
        sql: ${TABLE}.f_id ;;
      }

      dimension: Region_id{
        type: number
        sql: ${TABLE}.region_id ;;
      }

      dimension: star_level_id {
        type: number
        sql: ${TABLE}.star_level_id ;;
      }

      dimension: Franchise_name{
        type: string
        sql: ${TABLE}.display_name ;;
      }

#   dimension: call_entry_year{
#     type: number
#     sql: ${TABLE}.call_entry_year ;;
#   }
#
#   dimension: call_entry_quarter{
#     type: number
#     sql: ${TABLE}.call_entry_quarter ;;
#   }
#
#   dimension: call_entry_month{
#     type: number
#     sql: ${TABLE}.call_entry_month ;;
#   }
#
      dimension: times_called {
        type: string
        sql: ${TABLE}.times_called ;;
      }

      dimension: Month {
        type: string
        sql: ${TABLE}.Month ;;
      }

  dimension: Total_call_count {
    type: number
    sql: ${TABLE}.total_calls ;;
  }

  dimension: Coaching_call_count {
    type: number
    sql: ${TABLE}.total_cc ;;
  }

  dimension: atleast_2_calls {
    type: number
    sql: ${TABLE}.atleast_2_calls ;;
  }

  dimension: atleast_2_coaching_calls {
    type: number
    sql: ${TABLE}.atleast_2_coaching_calls ;;
  }

  dimension: occ {
    type: number
    sql: ${TABLE}.occ ;;
  }

  dimension: ncc {
    type: number
    sql: ${TABLE}.ncc ;;
  }

  dimension: never_called {
    type: number
    sql: ${TABLE}.never_called ;;
  }


  measure: total_call_count{
    type: sum
    sql: ${TABLE}.total_calls ;;
    #drill_fields: [franchise_detail*]
  }

  measure: total_coaching_call_count{
    type: sum
    sql: ${TABLE}.total_cc ;;
    #drill_fields: [franchise_detail*]
  }

  measure: 2plus_calls{
    type: sum
    sql: ${TABLE}.atleast_2_calls ;;
    filters: [atleast_2_calls: "1"]
    drill_fields: [franchises.franchise_detail*]
  }

  measure: 2_coaching_calls{
    type: sum
    sql: ${TABLE}.atleast_2_coaching_calls ;;
    filters: [atleast_2_coaching_calls: "1"]
    drill_fields: [franchises.franchise_detail*]
  }

  measure: 1_coaching_call{
    type: sum
    sql: ${TABLE}.occ ;;
    filters: [occ: "1"]
    drill_fields: [franchises.franchise_detail*]
  }

  measure: no_coaching_calls{
    type: sum
    sql: ${TABLE}.ncc ;;
    filters: [ncc: "1"]
    drill_fields: [franchises.franchise_detail*]
  }

  measure: no_calls{
    type: sum
    sql: ${TABLE}.never_called ;;
    filters: [never_called: "1"]
    drill_fields: [franchises.franchise_detail*]
  }




    }
