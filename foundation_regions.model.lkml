connection: "redshift"

# include all the views
include: "*.view"

# include all the dashboards
#include: "*.dashboard"

datagroup: foundation_regions_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: foundation_regions_default_datagroup

explore: franchises{
  label: "franchises-purchases-count"

  join: franchise_meta_data{
    type: left_outer
    sql_on: ${franchise_meta_data.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchises.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${franchises.franchise_region_id} = ${franchise_regions.id} ;;
    relationship: one_to_one
  }

sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
                  AND franchises.franchise_status_id IN (1,2)
                  AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
                  {% if _user_attributes['role'] == 'Coach' %}
                     AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
                  {% else %}
                     AND 1=1
                  {% endif %}
                  ;;


}

explore: franchise_addresses {
  label: "franchises-addresses"
  always_join: [franchise_regions]
  join: franchises{
    type: inner
    sql_on: ${franchise_addresses.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${franchises.franchise_region_id} = ${franchise_regions.id} ;;
    relationship: one_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
                         AND franchises.franchise_status_id IN (1,2)
                         AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
                         AND CASE WHEN franchise_addresses.primary_address THEN 'Yes' ELSE 'No' END  = 'Yes'
                        {% if _user_attributes['role'] == 'Coach' %}
                         AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
                        {% else %}
                         AND 1=1
                        {% endif %}
  ;;
hidden:  yes
  }

explore: agents {
  label: "franchises-producing"
  always_join: [franchises,reservations, franchise_regions]
  hidden:  yes
  join: franchises {
    type: inner
    sql_on: ${agents.franchise_id} =  ${franchises.id};;
    relationship: many_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchises.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${franchises.franchise_region_id} = ${franchise_regions.id} ;;
    relationship: one_to_one
  }

  join: reservations {
    type: inner
    sql_on: ${agents.id} =  ${reservations.agent_id};;
    relationship: one_to_many
  }

  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  sql_always_where:      CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
                         AND franchises.franchise_status_id IN (1,2)
                         AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
                         AND CASE WHEN agents.deleted THEN 'Yes' ELSE 'No' END  = 'No'
                         AND CASE WHEN agents.terminated THEN 'Yes' ELSE 'No' END  = 'No'
                         AND CASE WHEN reservations.deleted THEN 'Yes' ELSE 'No' END  = 'No'
                         {% if _user_attributes['role'] == 'Coach' %}
                         AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
                         {% else %}
                         AND 1=1
                         {% endif %}
  ;;


#   always_filter: {
#     filters: {
#       field: reservations.deleted
#       value: "No"
#     }
#   }
}

explore: franchises_non_producing {
  label: "franchises-non_producing"
  join: franchises {
    type: inner
    sql_on: ${franchises_non_producing.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchises.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${franchises.franchise_region_id} = ${franchise_regions.id} ;;
    relationship: one_to_one
  }

  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
  AND franchises.franchise_status_id IN (1,2)
  AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
  {% if _user_attributes['role'] == 'Coach' %}
    AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
  {% else %}
   AND 1=1
  {% endif %}
  ;;
  hidden: yes

}

explore: franchise_type_options {
  label: "franchises-type-option-count"
  hidden:  yes
  always_join: [franchise_regions]
  join: franchises {
    type: inner
    sql_on: ${franchise_type_options.id} =  ${franchises.franchise_type_option_id};;
    relationship: one_to_many
  }

  join: star_levels {
    type: inner
    sql_on: ${franchises.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${franchises.franchise_region_id} = ${franchise_regions.id} ;;
    relationship: one_to_one
  }

  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchises.id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
  AND franchises.franchise_status_id IN (1,2)
  AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
  {% if _user_attributes['role'] == 'Coach' %}
    AND franchises.franchise_region_id IN ({{ _user_attributes['region_id'] }},{{_user_attributes['parent_region_id']}})
  {% else %}
   AND 1=1
  {% endif %}
  ;;
}

explore: Franchise_train_booking_total {
  label: "frachises_allianz_goals"
  hidden:  yes
  join: Franchise_train_booking_allianz {
    type: left_outer
    sql_on: ${Franchise_train_booking_total.Total_linking} =  ${Franchise_train_booking_allianz.Allianz_linking};;
    relationship: one_to_one
  }

  join: Franchise_train_booking_cruise {
    type: left_outer
    sql_on: ${Franchise_train_booking_total.Total_linking} =  ${Franchise_train_booking_cruise.Cruise_linking};;
    relationship: one_to_one
  }

  join: franchise_owner_information {
    type: left_outer
    sql_on: ${Franchise_train_booking_total.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${Franchise_train_booking_total.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${Franchise_train_booking_total.Franchise_region_id} = ${franchise_regions.id} ;;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${Franchise_train_booking_total.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: {% if _user_attributes['role'] == 'Coach' %}
                     Franchise_train_booking_total.Region IN ({{ _user_attributes['region_id'] }},{{_user_attributes['parent_region_id']}})
                    {% else %}
                     1=1
                    {% endif %}
 ;;
}

explore: Franchise_train_count {
  label: "frachises_prod_goals"
  hidden:  yes
  join: Franchise_train_prod_3month {
    type: left_outer
    sql_on: ${Franchise_train_count.Just_linking} =  ${Franchise_train_prod_3month.3month_linking};;
    relationship: one_to_one
  }

  join: Franchise_train_prod_all {
    type: left_outer
    sql_on: ${Franchise_train_count.Just_linking} =  ${Franchise_train_prod_all.All_linking};;
    relationship: one_to_one
  }

  join: franchise_owner_information {
    type: left_outer
    sql_on: ${Franchise_train_count.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${Franchise_train_count.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${Franchise_train_count.Franchise_region_id} = ${franchise_regions.id} ;;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${Franchise_train_count.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: {% if _user_attributes['role'] == 'Coach' %}
  Franchise_train_count.Region IN ({{ _user_attributes['region_id'] }},{{_user_attributes['parent_region_id']}})
  {% else %}
  1=1
  {% endif %} ;;

}


explore: franchise_call_result {
  from:  franchises
  label: "frachises_call_goals"
  hidden:  yes
  join: franchise_call_status {
    type: left_outer
    sql_on: ${franchise_call_result.id} =  ${franchise_call_status.Franchise_id};;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: left_outer
    sql_on: ${franchise_call_status.Region_id} =  ${franchise_regions.id};;
    relationship: one_to_one
  }

  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchise_call_result.id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchise_call_result.id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  sql_always_where: CASE WHEN franchise_call_result.deleted THEN 'Yes' ELSE 'No' END  = 'No'
  AND franchise_call_result.franchise_status_id IN (1,2)
  AND cast(coalesce(nullif(franchise_call_result.contract_id,''), '0') as integer) >= 8000 ;;

}

explore : franchise_call_number_current {
  hidden: yes
  always_join: [franchise_regions]
  join: franchises {
    type: inner
    sql_on: ${franchise_call_number_current.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }
  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchise_call_number_current.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchise_call_number_current.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchise_call_number_current.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
  AND franchises.franchise_status_id IN (1,2)
  AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
  AND ${franchises.coaching_ready_date} IS NOT NULL
  {% if _user_attributes['role'] == 'Coach' %}
    AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
    AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                            from excolo.call_tracker_entries cte
                                            where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                  AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                  AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
                                            )
  {% else %}
    AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                            from excolo.call_tracker_entries cte
                                            where {% condition franchise_call_number_current.call_date %}cte.created{% endcondition %}
                                            AND EXTRACT(MONTH FROM cte.created) = (select max(EXTRACT(MONTH FROM cte.created))
                                                                                   from excolo.call_tracker_entries cte
                                                                                   where {% condition franchise_call_number_current.call_date %}cte.created{% endcondition %})
                                            )
  {% endif %}
  ;;
}


explore : coach_foundation_third_month_call {
  #hidden: yes
  always_join: [franchise_regions]
  join: franchises {
    type: inner
    sql_on: ${coach_foundation_third_month_call.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }
  join: franchise_owner_information {
    type: left_outer
    sql_on: ${coach_foundation_third_month_call.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${coach_foundation_third_month_call.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${coach_foundation_third_month_call.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
                                                )
      {% else %}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where {% condition coach_foundation_third_month_call.call_date %}cte.created{% endcondition %}
                                                AND EXTRACT(MONTH FROM cte.created) = (select max(EXTRACT(MONTH FROM cte.created))
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where {% condition coach_foundation_third_month_call.call_date %}cte.created{% endcondition %})
                                                )
      {% endif %}
      ;;
}


explore : franchise_call_number_last {
  hidden: yes
  join: franchises {
    type: inner
    sql_on: ${franchise_call_number_last.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }


join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchise_call_number_last.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchise_call_number_last.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchise_call_number_last.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
  AND franchises.franchise_status_id IN (1,2)
  AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
  AND ${franchises.coaching_ready_date} IS NOT NULL
  {% if _user_attributes['role'] == 'Coach' %}
    AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
    AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                            from excolo.call_tracker_entries cte
                                            where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                  AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                  AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 1
                                            )
  {% else %}
   AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                            from excolo.call_tracker_entries cte
                                            where {% condition franchise_call_number_last.call_date %}cte.created{% endcondition %}
                                            AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))+1
                                                                                   from excolo.call_tracker_entries cte
                                                                                   where {% condition franchise_call_number_last.call_date %}cte.created{% endcondition %})
                                            )
  {% endif %}
  ;;
}


explore : coach_foundation_second_month_call {
 # hidden: yes
  join: franchises {
    type: inner
    sql_on: ${coach_foundation_second_month_call.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }


  join: franchise_owner_information {
    type: left_outer
    sql_on: ${coach_foundation_second_month_call.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${coach_foundation_second_month_call.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${coach_foundation_second_month_call.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 1
                                                )
      {% else %}
       AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where {% condition coach_foundation_second_month_call.call_date %}cte.created{% endcondition %}
                                                AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))+1
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where {% condition coach_foundation_second_month_call.call_date %}cte.created{% endcondition %})
                                                )
      {% endif %}
      ;;
}

explore : franchise_call_number_last_to_last {
  join: franchises {
    type: inner
    sql_on: ${franchise_call_number_last_to_last.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }

hidden: yes
join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchise_call_number_last_to_last.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchise_call_number_last_to_last.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchise_call_number_last_to_last.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }


  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
  AND franchises.franchise_status_id IN (1,2)
  AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
  AND ${franchises.coaching_ready_date} IS NOT NULL
  {% if _user_attributes['role'] == 'Coach' %}
    AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
    AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                            from excolo.call_tracker_entries cte
                                            where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                  AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                  AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 2
                                            )
  {% else %}
   AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                            from excolo.call_tracker_entries cte
                                            where {% condition franchise_call_number_last_to_last.call_date %}cte.created{% endcondition %}
                                            AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))
                                                                                   from excolo.call_tracker_entries cte
                                                                                   where {% condition franchise_call_number_last_to_last.call_date %}cte.created{% endcondition %})
                                            )
  {% endif %}
  ;;
}


explore :  coach_foundation_first_month_call{
  join: franchises {
    type: inner
    sql_on: ${coach_foundation_first_month_call.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }

#  hidden: yes
  join: franchise_owner_information {
    type: left_outer
    sql_on: ${coach_foundation_first_month_call.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${coach_foundation_first_month_call.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${coach_foundation_first_month_call.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }


  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 2
                                                )
      {% else %}
       AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where {% condition coach_foundation_first_month_call.call_date %}cte.created{% endcondition %}
                                                AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where {% condition coach_foundation_first_month_call.call_date %}cte.created{% endcondition %})
                                                )
      {% endif %}
      ;;
}



explore : franchise_call_last2last_mill {
  join: franchises {
    type: inner
    sql_on: ${franchise_call_last2last_mill.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }

 # hidden: yes
  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchise_call_last2last_mill.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchise_call_last2last_mill.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchise_call_last2last_mill.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }


  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 2
                                                )
      {% else %}
       AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where ({% condition franchise_call_last2last_mill.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                AND {% condition franchise_call_last2last_mill.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %})
                                                AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where ({% condition franchise_call_last2last_mill.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                                                       AND {% condition franchise_call_last2last_mill.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %})
                                                                                      )
                                                )
      {% endif %}
      ;;
}


explore : coach_new_star_n_top_producer_first_month_call {
  join: franchises {
    type: inner
    sql_on: ${coach_new_star_n_top_producer_first_month_call.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }

  # hidden: yes
  join: franchise_owner_information {
    type: left_outer
    sql_on: ${coach_new_star_n_top_producer_first_month_call.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${coach_new_star_n_top_producer_first_month_call.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${coach_new_star_n_top_producer_first_month_call.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }


  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 2
                                                )
      {% else %}
       AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where ({% condition coach_new_star_n_top_producer_first_month_call.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                AND {% condition coach_new_star_n_top_producer_first_month_call.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %})
                                                AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where ({% condition coach_new_star_n_top_producer_first_month_call.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                                                       AND {% condition coach_new_star_n_top_producer_first_month_call.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %})
                                                                                      )
                                                )
      {% endif %}
      ;;
}

explore : franchise_call_last_mill {
 # hidden: yes
  join: franchises {
    type: inner
    sql_on: ${franchise_call_last_mill.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }


  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchise_call_last_mill.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchise_call_last_mill.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchise_call_last_mill.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 1
                                                )
      {% else %}
       AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where {% condition franchise_call_last_mill.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                      AND {% condition franchise_call_last_mill.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))+1
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where {% condition franchise_call_last_mill.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                                                              AND {% condition franchise_call_last_mill.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                                                      )
                                                )
      {% endif %}
      ;;
}

explore : coach_new_star_n_top_producer_second_month_call {
  # hidden: yes
  join: franchises {
    type: inner
    sql_on: ${coach_new_star_n_top_producer_second_month_call.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }


  join: franchise_owner_information {
    type: left_outer
    sql_on: ${coach_new_star_n_top_producer_second_month_call.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${coach_new_star_n_top_producer_second_month_call.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${coach_new_star_n_top_producer_second_month_call.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 1
                                                )
      {% else %}
       AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where {% condition coach_new_star_n_top_producer_second_month_call.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                      AND {% condition coach_new_star_n_top_producer_second_month_call.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                AND EXTRACT(MONTH FROM cte.created) = (select min(EXTRACT(MONTH FROM cte.created))+1
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where {% condition coach_new_star_n_top_producer_second_month_call.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                                                              AND {% condition coach_new_star_n_top_producer_second_month_call.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                                                      )
                                                )
      {% endif %}
      ;;
}


explore : franchise_call_current_mill {
  #hidden: yes
  always_join: [franchise_regions]
  join: franchises {
    type: inner
    sql_on: ${franchise_call_current_mill.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }
  join: franchise_owner_information {
    type: left_outer
    sql_on: ${franchise_call_current_mill.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${franchise_call_current_mill.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${franchise_call_current_mill.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
                                                )
      {% else %}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where {% condition franchise_call_current_mill.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                      AND {% condition franchise_call_current_mill.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                AND EXTRACT(MONTH FROM cte.created) = (select max(EXTRACT(MONTH FROM cte.created))
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where {% condition franchise_call_current_mill.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                                                              AND {% condition franchise_call_current_mill.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                                                      )
                                                )
      {% endif %}
      ;;
}

explore : coach_new_star_n_top_producer_third_month_call {
  #hidden: yes
  always_join: [franchise_regions]
  join: franchises {
    type: inner
    sql_on: ${coach_new_star_n_top_producer_third_month_call.franchise_id} =  ${franchises.id};;
    relationship: one_to_one
  }
  join: franchise_owner_information {
    type: left_outer
    sql_on: ${coach_new_star_n_top_producer_third_month_call.franchise_id} = ${franchise_owner_information.Franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_sales {
    type: left_outer
    sql_on: ${coach_new_star_n_top_producer_third_month_call.franchise_id} = ${franchise_sales.franchise_id} ;;
    relationship: one_to_one
  }

  join: franchise_regions {
    type: inner
    sql_on: ${franchise_regions.id} =  ${franchises.franchise_region_id};;
    relationship: one_to_one
  }

  join: star_levels {
    type: inner
    sql_on: ${coach_new_star_n_top_producer_third_month_call.star_level_id} = ${star_levels.id};;
    relationship: many_to_one
  }

  sql_always_where: CASE WHEN franchises.deleted THEN 'Yes' ELSE 'No' END  = 'No'
      AND franchises.franchise_status_id IN (1,2)
      AND cast(coalesce(nullif(franchises.contract_id,''), '0') as integer) >= 8000
      AND ${franchises.coaching_ready_date} IS NOT NULL
      {% if _user_attributes['role'] == 'Coach' %}
        AND franchises.franchise_region_id = {{ _user_attributes['region_id'] }}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
                                                      AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
                                                      AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
                                                )
      {% else %}
        AND ${franchises.coaching_ready_date} < (select DATE(min(cte.created))
                                                from excolo.call_tracker_entries cte
                                                where {% condition coach_new_star_n_top_producer_third_month_call.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                      AND {% condition coach_new_star_n_top_producer_third_month_call.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                AND EXTRACT(MONTH FROM cte.created) = (select max(EXTRACT(MONTH FROM cte.created))
                                                                                       from excolo.call_tracker_entries cte
                                                                                       where {% condition coach_new_star_n_top_producer_third_month_call.call_year %} EXTRACT (YEAR FROM cte.created) {% endcondition %}
                                                                                              AND {% condition coach_new_star_n_top_producer_third_month_call.call_quarter %} EXTRACT (QUARTER FROM cte.created) {% endcondition %}
                                                                                      )
                                                )
      {% endif %}
      ;;
}
