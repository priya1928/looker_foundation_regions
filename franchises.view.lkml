view: franchises {
  sql_table_name: excolo.franchises ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: allow_1099_download {
    type: yesno
    sql: ${TABLE}.allow_1099_download ;;
  }

  dimension: allow_associate_cohort_queue {
    type: yesno
    sql: ${TABLE}.allow_associate_cohort_queue ;;
  }

  dimension: allow_associate_comm_reports {
    type: yesno
    sql: ${TABLE}.allow_associate_comm_reports ;;
  }

  dimension: allow_associate_web_program {
    type: yesno
    sql: ${TABLE}.allow_associate_web_program ;;
  }

  dimension: ama_code {
    type: string
    sql: ${TABLE}.ama_code ;;
  }

  dimension: amex_gold_program {
    type: yesno
    sql: ${TABLE}.amex_gold_program ;;
  }

  dimension: auto_segment {
    type: yesno
    sql: ${TABLE}.auto_segment ;;
  }

  dimension: bank_account_number {
    type: string
    sql: ${TABLE}.bank_account_number ;;
  }

  dimension: bank_info_updated {
    type: yesno
    sql: ${TABLE}.bank_info_updated ;;
  }

  dimension: bank_routing_number {
    type: string
    sql: ${TABLE}.bank_routing_number ;;
  }

  dimension: bkg_cim_profile_id {
    type: string
    sql: ${TABLE}.bkg_cim_profile_id ;;
  }

  dimension: booking_code {
    type: string
    sql: ${TABLE}.booking_code ;;
  }

  dimension_group: brochure_rack_expiration {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.brochure_rack_expiration_date ;;
  }

  dimension_group: brochure_rack_next_billing {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.brochure_rack_next_billing_date ;;
  }

  dimension: brochure_rack_status {
    type: string
    sql: ${TABLE}.brochure_rack_status ;;
  }

  dimension: brochure_rack_trial_expiration_reminder_sent {
    type: number
    sql: ${TABLE}.brochure_rack_trial_expiration_reminder_sent ;;
  }

  dimension: business_entity_type {
    type: string
    sql: ${TABLE}.business_entity_type ;;
  }

  dimension: business_name {
    type: string
    sql: ${TABLE}.business_name ;;
  }

  dimension: cim_profile_id {
    type: string
    sql: ${TABLE}.cim_profile_id ;;
  }

  dimension: code {
    type: string
    sql: ${TABLE}.code ;;
  }

  dimension: cohort_free_allotment {
    type: number
    sql: ${TABLE}.cohort_free_allotment ;;
  }

  dimension: cohort_free_given {
    type: yesno
    sql: ${TABLE}.cohort_free_given ;;
  }

  dimension: commitment_program {
    type: number
    sql: ${TABLE}.commitment_program ;;
  }

  dimension: contact_number {
    type: string
    sql: ${TABLE}.contact_number ;;
  }

  dimension: contact_us_to {
    type: string
    sql: ${TABLE}.contact_us_to ;;
  }

  dimension: contract_id {
    type: number
    sql: cast(coalesce(nullif(${TABLE}.contract_id,''), '0') as integer) ;;
  }

  dimension: conversion {
    type: yesno
    sql: ${TABLE}.conversion ;;
  }

  dimension: converted_to_cpcentral {
    type: yesno
    sql: ${TABLE}.converted_to_cpcentral ;;
  }

  dimension: cp_percent {
    type: number
    sql: ${TABLE}.cp_percent ;;
  }

  dimension: cp_percent_low {
    type: number
    sql: ${TABLE}.cp_percent_low ;;
  }

  dimension: cp_share_percent {
    type: number
    sql: ${TABLE}.cp_share_percent ;;
  }

  dimension: cp_share_type {
    type: number
    sql: ${TABLE}.cp_share_type ;;
  }

  dimension: cpapi_password {
    type: string
    sql: ${TABLE}.cpapi_password ;;
  }

  dimension: cpapi_pcc {
    type: string
    sql: ${TABLE}.cpapi_pcc ;;
  }

  dimension: cpapi_user {
    type: string
    sql: ${TABLE}.cpapi_user ;;
  }

  dimension: cppod_new_agent_credit {
    type: number
    sql: ${TABLE}.cppod_new_agent_credit ;;
  }

  dimension: cpsoft_id {
    type: string
    sql: ${TABLE}.cpsoft_id ;;
  }

  dimension: cpsoft_status {
    type: string
    sql: ${TABLE}.cpsoft_status ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: did_not_attend_class {
    type: yesno
    sql: ${TABLE}.did_not_attend_class ;;
  }

  dimension: direct_deposit {
    type: yesno
    sql: ${TABLE}.direct_deposit ;;
  }

  dimension_group: disable_emails {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.disable_emails_on ;;
  }

  dimension: display_name {
    type: string
    sql: ${TABLE}.display_name ;;
  }

  dimension_group: downloaded_cc_list {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.downloaded_cc_list ;;
  }

  dimension: downloaded_cc_list_agent_id {
    type: number
    sql: ${TABLE}.downloaded_cc_list_agent_id ;;
  }

  dimension: ean_code {
    type: string
    sql: ${TABLE}.ean_code ;;
  }

  dimension: econnect_sync_on_display {
    type: yesno
    sql: ${TABLE}.econnect_sync_on_display ;;
  }

  dimension: emails_disabled {
    type: yesno
    sql: ${TABLE}.emails_disabled ;;
  }

  dimension: exclude_from_cppod {
    type: yesno
    sql: ${TABLE}.exclude_from_cppod ;;
  }

  dimension: expected_franchise_region_id {
    type: number
    sql: ${TABLE}.expected_franchise_region_id ;;
  }

  dimension_group: expected_transition {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.expected_transition_date ;;
  }

  dimension: fax {
    type: string
    sql: ${TABLE}.fax ;;
  }

  dimension: fdd_version {
    type: string
    sql: ${TABLE}.fdd_version ;;
  }

  dimension: franchise_default_reason {
    type: string
    sql: ${TABLE}.franchise_default_reason ;;
  }

  dimension: franchise_fee {
    type: number
    sql: ${TABLE}.franchise_fee ;;
  }

  dimension: franchise_incentive_level_id {
    type: number
    sql: ${TABLE}.franchise_incentive_level_id ;;
  }

  dimension: franchise_region_id {
    type: number
    sql: ${TABLE}.franchise_region_id ;;
  }

  dimension: franchise_salesperson_agent_id {
    type: number
    sql: ${TABLE}.franchise_salesperson_agent_id ;;
  }

  dimension: franchise_status_id {
    type: number
    sql: ${TABLE}.franchise_status_id ;;
  }

  dimension: franchise_type_notes {
    type: string
    sql: ${TABLE}.franchise_type_notes ;;
  }

  dimension: franchise_type_option_id {
    type: number
    sql: ${TABLE}.franchise_type_option_id ;;
  }

  dimension: fs_allotment {
    type: string
    sql: ${TABLE}.fs_allotment ;;
  }

  dimension_group: fs_date_enrolled {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.fs_date_enrolled ;;
  }

  dimension: fs_elite_plus_qty {
    type: number
    sql: ${TABLE}.fs_elite_plus_qty ;;
  }

  dimension: fs_elite_qty {
    type: number
    sql: ${TABLE}.fs_elite_qty ;;
  }

  dimension: full_sails_allotment_id {
    type: number
    sql: ${TABLE}.full_sails_allotment_id ;;
  }

  dimension: full_sails_elite_plus_allotment_id {
    type: number
    sql: ${TABLE}.full_sails_elite_plus_allotment_id ;;
  }

  dimension: full_sails_elite_plus_qty {
    type: number
    sql: ${TABLE}.full_sails_elite_plus_qty ;;
  }

  dimension: full_sails_elite_plus_status {
    type: string
    sql: ${TABLE}.full_sails_elite_plus_status ;;
  }

  dimension: full_sails_level {
    type: number
    sql: ${TABLE}.full_sails_level ;;
  }

  dimension: full_sails_qty {
    type: number
    sql: ${TABLE}.full_sails_qty ;;
  }

  dimension: full_sails_status {
    type: string
    sql: ${TABLE}.full_sails_status ;;
  }

  dimension: globus_email {
    type: string
    sql: ${TABLE}.globus_email ;;
  }

  dimension: home_office_news_email_address {
    type: string
    sql: ${TABLE}.home_office_news_email_address ;;
  }

  dimension_group: last_fcr_exported {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.last_fcr_exported ;;
  }

  dimension: leads_allowed {
    type: yesno
    sql: ${TABLE}.leads_allowed ;;
  }

  dimension: leads_certified {
    type: yesno
    sql: ${TABLE}.leads_certified ;;
  }

  dimension: leads_terms_accepted {
    type: yesno
    sql: ${TABLE}.leads_terms_accepted ;;
  }

  dimension_group: leads_terms_accepted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.leads_terms_accepted_date ;;
  }

  dimension: leads_to {
    type: string
    sql: ${TABLE}.leads_to ;;
  }

  dimension: local_phone {
    type: string
    sql: ${TABLE}.local_phone ;;
  }

  dimension: mkt_amex_logo_reason {
    type: string
    sql: ${TABLE}.mkt_amex_logo_reason ;;
  }

  dimension: mkt_amex_logo_usage {
    type: yesno
    sql: ${TABLE}.mkt_amex_logo_usage ;;
  }

  dimension: mobile_app_about_me {
    type: string
    sql: ${TABLE}.mobile_app_about_me ;;
  }

  dimension: mobile_app_show_address {
    type: yesno
    sql: ${TABLE}.mobile_app_show_address ;;
  }

  dimension: mobile_app_show_work_schedule {
    type: yesno
    sql: ${TABLE}.mobile_app_show_work_schedule ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension: name_on_check {
    type: string
    sql: ${TABLE}.name_on_check ;;
  }

  dimension_group: next_renewal {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.next_renewal_date ;;
  }

  dimension: occupation {
    type: string
    sql: ${TABLE}.occupation ;;
  }

  dimension: office_id {
    type: string
    sql: ${TABLE}.office_id ;;
  }

  dimension: orientation_coach_call_done {
    type: yesno
    sql: ${TABLE}.orientation_coach_call_done ;;
  }

  dimension: orientation_coach_email_exempt {
    type: yesno
    sql: ${TABLE}.orientation_coach_email_exempt ;;
  }

  dimension: orientation_exempt {
    type: yesno
    sql: ${TABLE}.orientation_exempt ;;
  }

  dimension_group: coaching_ready {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      month_num,
      quarter,
      year
    ]
    sql: CASE WHEN ${TABLE}.orientation_step1_completed and ${TABLE}.orientation_step2_completed and ${TABLE}.orientation_step3_completed
        THEN CASE WHEN ${TABLE}.orientation_step1_completed_date >= ${TABLE}.orientation_step2_completed_date
                 THEN CASE WHEN ${TABLE}.orientation_step3_completed_date >= ${TABLE}.orientation_step1_completed_date
                           THEN ${TABLE}.orientation_step3_completed_date
                           ELSE ${TABLE}.orientation_step1_completed_date
                      END
                 ELSE CASE WHEN ${TABLE}.orientation_step3_completed_date >= ${TABLE}.orientation_step2_completed_date
                           THEN ${TABLE}.orientation_step3_completed_date
                           ELSE ${TABLE}.orientation_step2_completed_date
                      END
            END
       ELSE NULL
  END ;;
  }

  dimension: orientation_step1_completed {
    type: yesno
    sql: ${TABLE}.orientation_step1_completed ;;
  }

  dimension_group: CTA_Orientation_Series {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orientation_step1_completed_date ;;
  }

  dimension_group: orientation_step1_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orientation_step1_completed_date ;;
  }

  dimension: orientation_step2_completed {
    type: yesno
    sql: ${TABLE}.orientation_step2_completed ;;
  }

  dimension_group: Franchise_Credit_Card {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orientation_step2_completed_date ;;
  }

  dimension_group: orientation_step2_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orientation_step2_completed_date ;;
  }

  dimension: orientation_step3_completed {
    type: yesno
    sql: ${TABLE}.orientation_step3_completed ;;
  }

  dimension_group: W9_Submitted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orientation_step3_completed_date ;;
  }

  dimension_group: orientation_step3_completed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.orientation_step3_completed_date ;;
  }

  dimension: pay_immediately {
    type: yesno
    sql: ${TABLE}.pay_immediately ;;
  }

  dimension: postcard_from_address {
    type: string
    sql: ${TABLE}.postcard_from_address ;;
  }

  dimension: postcard_from_name {
    type: string
    sql: ${TABLE}.postcard_from_name ;;
  }

  dimension: primary_email {
    type: string
    sql: ${TABLE}.primary_email ;;
  }

  dimension_group: protege_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.protege_end_date ;;
  }

  dimension: receive_optin_notifications {
    type: number
    sql: ${TABLE}.receive_optin_notifications ;;
  }

  dimension: recognition_name {
    type: string
    sql: ${TABLE}.recognition_name ;;
  }

  dimension: referral_agent_id {
    type: number
    sql: ${TABLE}.referral_agent_id ;;
  }

  dimension_group: referral_bonus_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.referral_bonus_end_date ;;
  }

  dimension: referral_name {
    type: string
    sql: ${TABLE}.referral_name ;;
  }

  dimension_group: renewed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.renewed ;;
  }

  dimension_group: resigned {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.resigned ;;
  }

  dimension: ringing_in_the_leads {
    type: yesno
    sql: ${TABLE}.ringing_in_the_leads ;;
  }

  dimension_group: satori_batch_declined {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.satori_batch_declined ;;
  }

  dimension_group: satori_batch_processed {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.satori_batch_processed ;;
  }

  dimension_group: satori_validation_results_downloaded {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.satori_validation_results_downloaded ;;
  }

  dimension: sendible_content_type {
    type: string
    sql: ${TABLE}.sendible_content_type ;;
  }

  dimension_group: sendible_expiration {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sendible_expiration_date ;;
  }

  dimension: sendible_facebook_page {
    type: string
    sql: ${TABLE}.sendible_facebook_page ;;
  }

  dimension_group: sendible_next_billing {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.sendible_next_billing_date ;;
  }

  dimension: sendible_status {
    type: string
    sql: ${TABLE}.sendible_status ;;
  }

  dimension: show_in_agent_search {
    type: yesno
    sql: ${TABLE}.show_in_agent_search ;;
  }

  dimension: star_level_id {
    type: number
    sql: ${TABLE}.star_level_id ;;
  }

  dimension_group: start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.start_date ;;
  }

  dimension: tax_id {
    type: string
    sql: ${TABLE}.tax_id ;;
  }

  dimension_group: termination {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.termination_date ;;
  }

  dimension: termination_reason_id {
    type: number
    sql: ${TABLE}.termination_reason_id ;;
  }

  dimension: ticketmonster_aid {
    type: number
    value_format_name: id
    sql: ${TABLE}.ticketmonster_aid ;;
  }

  dimension: ticketmonster_partner_email {
    type: string
    sql: ${TABLE}.ticketmonster_partner_email ;;
  }

  dimension: ticketmonster_partner_password {
    type: string
    sql: ${TABLE}.ticketmonster_partner_password ;;
  }

  dimension: toll_free_phone {
    type: string
    sql: ${TABLE}.toll_free_phone ;;
  }

  dimension_group: training {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.training_date ;;
  }

  dimension: travel_insider {
    type: yesno
    sql: ${TABLE}.travel_insider ;;
  }

  dimension: vanity_phone {
    type: string
    sql: ${TABLE}.vanity_phone ;;
  }

  dimension: viking_code {
    type: string
    sql: ${TABLE}.viking_code ;;
  }

  dimension: w9_reminders {
    type: number
    sql: ${TABLE}.w9_reminders ;;
  }

  dimension_group: wp_fees_waived_end {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wp_fees_waived_end ;;
  }

  dimension_group: wp_fees_waived_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wp_fees_waived_start ;;
  }

  dimension: wp_level {
    type: number
    sql: ${TABLE}.wp_level ;;
  }

  dimension_group: wp_start {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.wp_start ;;
  }

  measure: count {
    type: count
    drill_fields: [franchise_detail*]
  }


   # ----- Sets of fields for drilling ------
  set: franchise_detail {
    fields: [
      franchises.contract_id,
      franchise_owner_information.Owner_name,
      franchise_owner_information.last_valid_coaching_call,
      franchise_owner_information.Contract_Signed,
      coaching_ready_date,
      CTA_Orientation_Series_date,
      Franchise_Credit_Card_date,
      W9_Submitted_date,
      star_levels.Star_Level,
      franchise_owner_information.Owner_email,
      franchise_owner_information.Work_phone,
      franchise_owner_information.Training_date,
      franchise_sales.Total_sales

    ]
  }
}
