view: reservations {
  sql_table_name: excolo.reservations ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: _sdc_batched {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_batched_at ;;
  }

  dimension_group: _sdc_extracted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_extracted_at ;;
  }

  dimension_group: _sdc_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_received_at ;;
  }

  dimension: _sdc_sequence {
    type: number
    sql: ${TABLE}._sdc_sequence ;;
  }

  dimension: _sdc_table_version {
    type: number
    sql: ${TABLE}._sdc_table_version ;;
  }

  dimension: agent_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.agent_id ;;
  }

  dimension: amex_gold_exported {
    type: yesno
    sql: ${TABLE}.amex_gold_exported ;;
  }

  dimension: amex_tip_code_id {
    type: number
    sql: ${TABLE}.amex_tip_code_id ;;
  }

  dimension: amex_tip_exported {
    type: yesno
    sql: ${TABLE}.amex_tip_exported ;;
  }

  dimension: client_id {
    type: number
    sql: ${TABLE}.client_id ;;
  }

  dimension: confirmation_number {
    type: string
    sql: ${TABLE}.confirmation_number ;;
  }

  dimension: cpsoft_agtcode {
    type: string
    sql: ${TABLE}.cpsoft_agtcode ;;
  }

  dimension: cpsoft_timestamp {
    type: string
    sql: ${TABLE}.cpsoft_timestamp ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: default_invoice_params {
    type: string
    sql: ${TABLE}.default_invoice_params ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension_group: departure {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.departure_date ;;
  }

  dimension: destination {
    type: string
    sql: ${TABLE}.destination ;;
  }

  dimension: franchise_list_id {
    type: number
    sql: ${TABLE}.franchise_list_id ;;
  }

  dimension_group: ho_locked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.ho_locked ;;
  }

  dimension_group: locked {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.locked ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension_group: return {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.return_date ;;
  }

  dimension: ship {
    type: string
    sql: ${TABLE}.ship ;;
  }

  dimension: source_id {
    type: number
    sql: ${TABLE}.source_id ;;
  }

  dimension: travel_insider {
    type: yesno
    sql: ${TABLE}.travel_insider ;;
  }

  dimension: travel_insider_exported {
    type: yesno
    sql: ${TABLE}.travel_insider_exported ;;
  }

  dimension: vendor {
    type: string
    sql: ${TABLE}.vendor ;;
  }

  dimension: waived_travel_protection {
    type: yesno
    sql: ${TABLE}.waived_travel_protection ;;
  }

  dimension: website_id {
    type: number
    sql: ${TABLE}.website_id ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  # ----- Sets of fields for drilling ------
  set: detail {
    fields: [
      id,
      agents.id,
      agents.twitter_username,
      agents.trip_summary_username,
      agents.other_name,
      agents.nickname,
      agents.last_name,
      agents.first_name,
      agents.facebook_fanpage_name,
      agents.display_name
    ]
  }
}
