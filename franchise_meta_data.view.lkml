view: franchise_meta_data {
  sql_table_name: excolo_full.franchise_meta_data ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: call_tracker_last_called {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.call_tracker_last_called ;;
  }

  dimension: call_tracker_last_called_agent {
    type: string
    sql: ${TABLE}.call_tracker_last_called_agent ;;
  }

  dimension: call_tracker_last_called_id {
    type: number
    sql: ${TABLE}.call_tracker_last_called_id ;;
  }

  dimension_group: call_tracker_last_coaching_call {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.call_tracker_last_coaching_call ;;
  }

  dimension: call_tracker_last_coaching_call_agent {
    type: string
    sql: ${TABLE}.call_tracker_last_coaching_call_agent ;;
  }

  dimension: call_tracker_last_coaching_call_id {
    type: number
    sql: ${TABLE}.call_tracker_last_coaching_call_id ;;
  }

  dimension: departures_current_month_sales {
    type: number
    sql: ${TABLE}.departures_current_month_sales ;;
  }

  dimension: departures_current_year_sales {
    type: number
    sql: ${TABLE}.departures_current_year_sales ;;
  }

  dimension: departures_last_12_month_sales {
    type: number
    sql: ${TABLE}.departures_last_12_month_sales ;;
  }

  dimension: departures_last_6_month_sales {
    type: number
    sql: ${TABLE}.departures_last_6_month_sales ;;
  }

  dimension: departures_last_6_week_sales {
    type: number
    sql: ${TABLE}.departures_last_6_week_sales ;;
  }

  dimension: departures_last_month_sales {
    type: number
    sql: ${TABLE}.departures_last_month_sales ;;
  }

  dimension: departures_last_week_sales {
    type: number
    sql: ${TABLE}.departures_last_week_sales ;;
  }

  dimension: departures_last_year_current_month_sales {
    type: number
    sql: ${TABLE}.departures_last_year_current_month_sales ;;
  }

  dimension: departures_last_year_prior_month_sales {
    type: number
    sql: ${TABLE}.departures_last_year_prior_month_sales ;;
  }

  dimension: departures_last_year_sales {
    type: number
    sql: ${TABLE}.departures_last_year_sales ;;
  }

  dimension: departures_last_year_to_date_sales {
    type: number
    sql: ${TABLE}.departures_last_year_to_date_sales ;;
  }

  dimension: departures_next_year_sales {
    type: number
    sql: ${TABLE}.departures_next_year_sales ;;
  }

  dimension: departures_next_year_to_date_sales {
    type: number
    sql: ${TABLE}.departures_next_year_to_date_sales ;;
  }

  dimension: departures_year_to_date_sales {
    type: number
    sql: ${TABLE}.departures_year_to_date_sales ;;
  }

  dimension: franchise_id {
    type: number
    sql: ${TABLE}.franchise_id ;;
  }

  dimension: millionaire {
    type: yesno
    sql: ${TABLE}.millionaire ;;
  }

  dimension: previous_millionaire {
    type: yesno
    sql: ${TABLE}.previous_millionaire ;;
  }

  dimension: previous_star_level_id {
    type: number
    sql: ${TABLE}.previous_star_level_id ;;
  }

  dimension: purchases_current_month_sales {
    type: number
    sql: ${TABLE}.purchases_current_month_sales ;;
  }

  dimension: purchases_current_month_sales_sum {
    type: number
    sql: sum(${TABLE}.purchases_current_month_sales) ;;
  }

  dimension: purchases_last_12_month_sales {
    type: number
    sql: ${TABLE}.purchases_last_12_month_sales ;;
  }

  dimension: purchases_last_6_month_sales {
    type: number
    sql: ${TABLE}.purchases_last_6_month_sales ;;
  }

  dimension: purchases_last_6_week_sales {
    type: number
    sql: ${TABLE}.purchases_last_6_week_sales ;;
  }

 dimension: purchases_last_month_sales {
    type: number
    sql: ${TABLE}.purchases_last_month_sales ;;
  }

  dimension: purchases_last_month_sales_sum {
    type: number
    sql: sum(${TABLE}.purchases_last_month_sales) ;;
  }

  dimension: sales_sum_diff {
    type: number
    sql: sum(${TABLE}.purchases_current_month_sales) - sum(${TABLE}.purchases_last_month_sales) ;;
  }

  dimension: sales_sum_var{
    type: number
    sql: (sum(${TABLE}.purchases_current_month_sales) / sum(${TABLE}.purchases_last_month_sales)) -1  ;;
  }

  dimension: purchases_last_week_sales {
    type: number
    sql: ${TABLE}.purchases_last_week_sales ;;
  }

  dimension: purchases_last_year_current_month_sales {
    type: number
    sql: ${TABLE}.purchases_last_year_current_month_sales ;;
  }

  dimension: purchases_last_year_prior_month_sales {
    type: number
    sql: ${TABLE}.purchases_last_year_prior_month_sales ;;
  }

  dimension: purchases_last_year_sales {
    type: number
    sql: ${TABLE}.purchases_last_year_sales ;;
  }

  dimension: purchases_last_year_to_date_sales {
    type: number
    sql: ${TABLE}.purchases_last_year_to_date_sales ;;
  }

  dimension: purchases_year_to_date_sales {
    type: number
    sql: ${TABLE}.purchases_year_to_date_sales ;;
  }


  measure: sum_current_month_sales {
    type: sum
    sql:  ${TABLE}.purchases_current_month_sales ;;
    drill_fields: [franchise_detail*]
  }

  measure: sum_last_month_sales {
    type: sum
    sql:  ${TABLE}.purchases_last_month_sales ;;
    drill_fields: [franchise_detail*]
  }
  set: franchise_detail {
    fields: [
      franchises.contract_id,
      franchise_owner_information.Owner_name,
      franchise_owner_information.Work_phone,
      franchise_owner_information.Training_date,
      franchise_owner_information.last_coaching_call,
      franchise_meta_data.departures_year_to_date_sales
    ]
  }
}
