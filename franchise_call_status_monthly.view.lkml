view: franchise_call_status_monthly {
  derived_table: {
    sql:SELECT f.franchise_region_id AS region
  , f.id AS "franchise_id"
  , COALESCE(month1.score,0) AS last_to_last_month_score
  , CASE WHEN COALESCE(month1.score,0) = 1 THEN cast('true' as BOOL) ELSE cast('false' as BOOL) END AS "called_twice_last_to_last_month"
  , COALESCE(month2.score,0) AS last_month_score_score
  , CASE WHEN COALESCE(month2.score,0) = 1 THEN cast('true' as BOOL) ELSE cast('false' as BOOL) END AS "called_twice_last_month"
  , COALESCE(month3.score,0) AS current_month_score
  , CASE WHEN COALESCE(month3.score,0) = 1 THEN cast('true' as BOOL) ELSE cast('false' as BOOL) END AS "called_twice_current_month"
  , CASE WHEN COALESCE(month1.score,0)+ COALESCE(month2.score,0)+ COALESCE(month3.score,0)=3 THEN  1 ELSE 0 END AS score_total
  , CASE WHEN COALESCE(month1.score,0)+ COALESCE(month2.score,0)+ COALESCE(month3.score,0)=3 THEN  cast('true' as BOOL) ELSE cast('false' as BOOL) END AS "Quarter_Target_Met"
  FROM excolo.franchises f

  LEFT JOIN
    (
    SELECT f.id
      ,CASE WHEN f.franchise_region_id=10
           THEN CASE WHEN COUNT(cte.id)>=1 THEN 1 ELSE 0 END -- Rock  must achieve at least one call per month
           ELSE CASE WHEN COUNT(cte.id)>=2 THEN 1 ELSE 0 END
           END AS score
     -- All other foundation must achieve two calls
    FROM excolo.call_tracker_entries cte
    INNER JOIN excolo.agents a ON a.id=cte.agent_id AND a.deleted=0 -- and a.terminated=0
    INNER JOIN excolo.franchises f ON f.id=a.franchise_id AND f.deleted=0 AND f.franchise_status_id IN (1,2) AND CAST(f.contract_id AS integer)>=8000 -- AND f.`franchise_region_id`=2
    WHERE EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
    AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
    AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 2
    AND cte.call_type IN ('inbound','inbound_coaching','outbound','outbound_coaching')
    AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14)
    GROUP BY f.id, f.franchise_region_id
    ORDER BY f.id, f.franchise_region_id
    ) month1 ON month1.id=f.id
  LEFT JOIN
    (
    SELECT f.id
      ,CASE WHEN f.franchise_region_id=10
           THEN CASE WHEN COUNT(cte.id)>=1 THEN 1 ELSE 0 END -- Rock  must achieve at least one call per month
           ELSE CASE WHEN COUNT(cte.id)>=2 THEN 1 ELSE 0 END
           END AS score
     -- All other foundation must achieve two calls
    FROM excolo.call_tracker_entries cte
    INNER JOIN excolo.agents a ON a.id=cte.agent_id AND a.deleted=0 -- and a.terminated=0
    INNER JOIN excolo.franchises f ON f.id=a.franchise_id AND f.deleted=0 AND f.franchise_status_id IN (1,2) AND CAST(f.contract_id AS integer)>=8000 -- AND f.`franchise_region_id`=2
    WHERE EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
    AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
    AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE) - 1
    AND cte.call_type IN ('inbound','inbound_coaching','outbound','outbound_coaching')
    AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14)
    GROUP BY f.id, f.franchise_region_id
    ORDER BY f.id, f.franchise_region_id
    ) month2 ON month2.id=f.id
  LEFT JOIN
    (
    SELECT f.id
      ,CASE WHEN f.franchise_region_id=10
           THEN CASE WHEN COUNT(cte.id)>=1 THEN 1 ELSE 0 END -- Rock  must achieve at least one call per month
           ELSE CASE WHEN COUNT(cte.id)>=2 THEN 1 ELSE 0 END
           END AS score
     -- All other foundation must achieve two calls
    FROM excolo.call_tracker_entries cte
    INNER JOIN excolo.agents a ON a.id=cte.agent_id AND a.deleted=0 -- and a.terminated=0
    INNER JOIN excolo.franchises f ON f.id=a.franchise_id AND f.deleted=0 AND f.franchise_status_id IN (1,2) AND CAST(f.contract_id AS integer)>=8000 -- AND f.`franchise_region_id`=2
    WHERE EXTRACT(YEAR FROM cte.created)=EXTRACT(YEAR FROM CURRENT_DATE)
    AND EXTRACT(QUARTER FROM cte.created)=EXTRACT(QUARTER FROM CURRENT_DATE)
    AND EXTRACT(MONTH FROM cte.created)=EXTRACT(MONTH FROM CURRENT_DATE)
    AND cte.call_type IN ('inbound','inbound_coaching','outbound','outbound_coaching')
    AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14)
    GROUP BY f.id, f.franchise_region_id
    ORDER BY f.id, f.franchise_region_id
    ) month3 ON month3.id=f.id
  WHERE f.deleted=0 AND f.franchise_status_id IN (1,2) AND CAST(f.contract_id AS integer)>=8000
  AND f.franchise_region_id in (1,2,3,4,5,6,7,8,9,10,11,12,13,14)   -- A dirty Trick for a stupid error that was coming
  AND f.star_level_id IN (1,2,3,4,5,6,9) -- Millionaire(7) Not Included
  -- and f.id=953
  LIMIT 3000

                   ;;
   # persist_for: "1 hours" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours
    #distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

  }
  # DIMENSIONS #

  dimension: Franchise_id {
    primary_key: yes
    type: number
    sql: ${TABLE}.franchise_id ;;
  }

  dimension: Region_id{
    type: number
    sql: ${TABLE}.region ;;
  }

  dimension: called_twice_current_month {
    type: yesno
    sql: ${TABLE}.called_twice_current_month ;;
  }

  dimension: called_twice_last_month {
    type: yesno
    sql: ${TABLE}.called_twice_last_month ;;
  }

  dimension: called_twice_last_to_last_month {
    type: yesno
    sql: ${TABLE}.called_twice_last_to_last_month ;;
  }

  dimension: Quarter_Target_Met {
    type: yesno
    sql: ${TABLE}.Quarter_Target_Met ;;
  }

#   measure: f_count{
#     type: count_distinct
#     sql:   ${TABLE}.franchise_id ;;
#     drill_fields: [Franchise_id]
#   }
#
#   measure: sum_curr_month {
#     type: sum
#     sql:  ${TABLE}.current_month_score ;;
#     drill_fields: [Franchise_id]
#   }
#
#   measure: sum_last_month {
#     type: sum
#     sql:  ${TABLE}.last_month_score ;;
#     drill_fields: [Franchise_id]
#   }
#
#   measure: sum_last_to_last_month {
#     type: sum
#     sql:  ${TABLE}.last_to_last_month_score ;;
#     drill_fields: [Franchise_id]
#   }
#
#   measure: sum_total{
#     type: sum
#     sql:  ${TABLE}.score_total ;;
#     drill_fields: [Franchise_id]
#   }


}
