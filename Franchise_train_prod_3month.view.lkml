view: Franchise_train_prod_3month {
  derived_table: {
    sql:SELECT COUNT(DISTINCT f.id) AS Three_Month_count
        , f.id AS franchise_id
        , f.franchise_region_id AS Region
        , f.training_date AS Training_Date
        , EXTRACT(YEAR FROM f.training_date) AS Training_Year
        , EXTRACT(MONTH FROM f.training_date) AS Training_Month
        , CONCAT(f.id,CONCAT(f.franchise_region_id,CONCAT(EXTRACT(YEAR FROM f.training_date),EXTRACT(MONTH FROM f.training_date)))) AS linking
        FROM excolo.agents a

        INNER JOIN excolo.reservations r
          ON r.agent_id=a.id
          AND r.deleted=0 AND a.deleted=0 AND a.terminated=0

        INNER JOIN excolo.franchises f
          ON a.franchise_id=f.id AND f.deleted=0 AND f.franchise_status_id IN (1,2) AND cast(coalesce(nullif(f.contract_id,''), '0') as integer)>=8000

          AND f.franchise_region_id IN (1,2,3,4,5,6,7,8,9,10,11,12,13,14)

          AND (r.created - f.training_date)<90


        GROUP BY f.training_date, f.franchise_region_id, f.id
        ORDER BY f.training_date DESC, f.franchise_region_id DESC
             ;;
   # persist_for: "40 minutes" #Indicates to 'persist' this to the db as a PDT.  I.e. create table.  Table will rebuild after 12 hours
   # distribution_style: all
    #     sql_trigger_value: SELECT CURDATE()  ##This is an alternate mechanism to persist a table

  }
  # DIMENSIONS #
  dimension: Three_Month_count {
    type: number
    sql: ${TABLE}. Three_Month_count ;;
  }

  dimension: franchise_id{
    type: number
    primary_key: yes
    sql: ${TABLE}. franchise_id ;;
  }

  dimension: Training_Date {
    type: date
    sql: ${TABLE}.Training_Date ;;
  }

  dimension: Training_Class {
    type: string
    sql: CONCAT(${TABLE}.Training_Month,CONCAT('/',${TABLE}.Training_Year)) ;;
  }

  dimension: Franchise_region_id {
    type: number
    sql: ${TABLE}.Region ;;
  }

  dimension: 3month_linking {
    type: string
    sql: ${TABLE}.linking ;;
  }

  measure: sum_Three_Month_count {
    type: sum
    sql:  ${TABLE}.Three_Month_count ;;
    drill_fields: [franchise_detail*]
  }

  set: franchise_detail {
    fields: [
      franchises.contract_id,
      franchise_owner_information.Owner_name,
      star_levels.Star_Level,
      franchise_owner_information.Work_phone,
      franchise_owner_information.Training_date,
      franchise_owner_information.last_coaching_call,
      franchise_sales.Total_sales

    ]
  }

}
