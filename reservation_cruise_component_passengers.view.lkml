view: reservation_cruise_component_passengers {
  sql_table_name: excolo.reservation_cruise_component_passengers ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: _sdc_batched {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_batched_at ;;
  }

  dimension_group: _sdc_extracted {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_extracted_at ;;
  }

  dimension_group: _sdc_received {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}._sdc_received_at ;;
  }

  dimension: _sdc_sequence {
    type: number
    sql: ${TABLE}._sdc_sequence ;;
  }

  dimension: _sdc_table_version {
    type: number
    sql: ${TABLE}._sdc_table_version ;;
  }

  dimension: additional {
    type: number
    sql: ${TABLE}.additional ;;
  }

  dimension: addon_commission {
    type: number
    sql: ${TABLE}.addon_commission ;;
  }

  dimension: addon_commission_percentage {
    type: number
    sql: ${TABLE}.addon_commission_percentage ;;
  }

  dimension: addon_price {
    type: number
    sql: ${TABLE}.addon_price ;;
  }

  dimension: addon_price_commissionable {
    type: yesno
    sql: ${TABLE}.addon_price_commissionable ;;
  }

  dimension: administrative_fees {
    type: number
    sql: ${TABLE}.administrative_fees ;;
  }

  dimension: air_commission {
    type: number
    sql: ${TABLE}.air_commission ;;
  }

  dimension: air_markup {
    type: number
    sql: ${TABLE}.air_markup ;;
  }

  dimension: air_price {
    type: number
    sql: ${TABLE}.air_price ;;
  }

  dimension: air_price_commissionable {
    type: yesno
    sql: ${TABLE}.air_price_commissionable ;;
  }

  dimension: air_tc {
    type: yesno
    sql: ${TABLE}.air_tc ;;
  }

  dimension: beverage_package {
    type: number
    sql: ${TABLE}.beverage_package ;;
  }

  dimension: beverage_package_commission {
    type: number
    sql: ${TABLE}.beverage_package_commission ;;
  }

  dimension: beverage_package_commissionable {
    type: yesno
    sql: ${TABLE}.beverage_package_commissionable ;;
  }

  dimension: bonus_commission {
    type: number
    sql: ${TABLE}.bonus_commission ;;
  }

  dimension: cancelled {
    type: yesno
    sql: ${TABLE}.cancelled ;;
  }

  dimension: client_id {
    type: number
    sql: ${TABLE}.client_id ;;
  }

  dimension: commission {
    type: number
    sql: ${TABLE}.commission ;;
  }

  dimension: commission_percentage {
    type: number
    sql: ${TABLE}.commission_percentage ;;
  }

  dimension: coupon {
    type: number
    sql: ${TABLE}.coupon ;;
  }

  dimension: cpsoft_position {
    type: number
    sql: ${TABLE}.cpsoft_position ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created ;;
  }

  dimension: deleted {
    type: yesno
    sql: ${TABLE}.deleted ;;
  }

  dimension: discount {
    type: number
    sql: ${TABLE}.discount ;;
  }

  dimension: fuel_charge {
    type: number
    sql: ${TABLE}.fuel_charge ;;
  }

  dimension: gateway {
    type: string
    sql: ${TABLE}.gateway ;;
  }

  dimension: government_taxes {
    type: number
    sql: ${TABLE}.government_taxes ;;
  }

  dimension: gratuities {
    type: number
    sql: ${TABLE}.gratuities ;;
  }

  dimension: gratuities_commission {
    type: number
    sql: ${TABLE}.gratuities_commission ;;
  }

  dimension: gratuities_commissionable {
    type: yesno
    sql: ${TABLE}.gratuities_commissionable ;;
  }

  dimension: insurance_commission {
    type: number
    sql: ${TABLE}.insurance_commission ;;
  }

  dimension: insurance_commission_percentage {
    type: number
    sql: ${TABLE}.insurance_commission_percentage ;;
  }

  dimension: insurance_fee {
    type: number
    sql: ${TABLE}.insurance_fee ;;
  }

  dimension: insurance_price {
    type: number
    sql: ${TABLE}.insurance_price ;;
  }

  dimension: insurance_price_commissionable {
    type: yesno
    sql: ${TABLE}.insurance_price_commissionable ;;
  }

  dimension: internet_access {
    type: number
    sql: ${TABLE}.internet_access ;;
  }

  dimension: internet_access_commission {
    type: number
    sql: ${TABLE}.internet_access_commission ;;
  }

  dimension: internet_access_commissionable {
    type: yesno
    sql: ${TABLE}.internet_access_commissionable ;;
  }

  dimension: markup {
    type: number
    sql: ${TABLE}.markup ;;
  }

  dimension_group: modified {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.modified ;;
  }

  dimension: ncf {
    type: number
    sql: ${TABLE}.ncf ;;
  }

  dimension: other_charges {
    type: number
    sql: ${TABLE}.other_charges ;;
  }

  dimension: package_commission {
    type: number
    sql: ${TABLE}.package_commission ;;
  }

  dimension: paid_with_tc {
    type: yesno
    sql: ${TABLE}.paid_with_tc ;;
  }

  dimension: price {
    type: number
    sql: ${TABLE}.price ;;
  }

  dimension: pwtc_orig_commission {
    type: number
    sql: ${TABLE}.pwtc_orig_commission ;;
  }

  dimension: pwtc_orig_discount {
    type: number
    sql: ${TABLE}.pwtc_orig_discount ;;
  }

  dimension: pwtc_orig_price {
    type: number
    sql: ${TABLE}.pwtc_orig_price ;;
  }

  dimension: reservation_cruise_component_id {
    type: number
    sql: ${TABLE}.reservation_cruise_component_id ;;
  }

  dimension: service_fees {
    type: number
    sql: ${TABLE}.service_fees ;;
  }

  dimension: spa_package {
    type: number
    sql: ${TABLE}.spa_package ;;
  }

  dimension: spa_package_commission {
    type: number
    sql: ${TABLE}.spa_package_commission ;;
  }

  dimension: spa_package_commissionable {
    type: yesno
    sql: ${TABLE}.spa_package_commissionable ;;
  }

  dimension: specialty_dining {
    type: number
    sql: ${TABLE}.specialty_dining ;;
  }

  dimension: specialty_dining_commission {
    type: number
    sql: ${TABLE}.specialty_dining_commission ;;
  }

  dimension: specialty_dining_commissionable {
    type: yesno
    sql: ${TABLE}.specialty_dining_commissionable ;;
  }

  dimension: tc {
    type: yesno
    sql: ${TABLE}.tc ;;
  }

  dimension: tc_amount {
    type: number
    sql: ${TABLE}.tc_amount ;;
  }

  dimension: tc_discount {
    type: number
    sql: ${TABLE}.tc_discount ;;
  }

  dimension: transfers {
    type: number
    sql: ${TABLE}.transfers ;;
  }

  dimension: transfers_commission {
    type: number
    sql: ${TABLE}.transfers_commission ;;
  }

  dimension: transfers_commissionable {
    type: yesno
    sql: ${TABLE}.transfers_commissionable ;;
  }

  dimension: travel_visa_fee {
    type: number
    sql: ${TABLE}.travel_visa_fee ;;
  }

  measure: count {
    type: count
    drill_fields: [id]
  }
}
